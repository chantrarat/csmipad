
import { createDrawerNavigator, DrawerItems } from 'react-navigation'
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  Dimensions,
  Image
} from 'react-native';

//redux-------------------------------------------------------------------
// import { combineReducers } from 'redux'
// import reducers from './src/Reducer'
// import { createStore, applyMiddleware, compose } from 'redux'
// import thunk from 'redux-thunk'
// import logger from 'redux-logger'
// import { Provider } from 'react-redux'
//redux-------------------------------------------------------------------

import { MenuProvider } from 'react-native-popup-menu';
import saleScreen from './src/component/sale'
import orderScreen from './src/component/order'
import customerScreen from './src/component/customer'
import manScreen from './src/component/man'
import statisticScreen from './src/component/statistic'
// import logoutScreen from './Logout'
// import { connect } from 'react-redux'
// import { getAll } from './src/Action/index'

// const {width}= Dimensions.get('screen')

//redux-------------------------------------------------------------------
// const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
// const reducer = combineReducers({ reducers })
// const store = createStore(
//   reducer,
//   composeEnhancer(applyMiddleware(thunk, logger))
//   // composeEnhancer(applyMiddleware(thunk))     // กรณีไม่ต้องการให้ log เมื่อ state redux เปลี่ยน
// )
//redux-------------------------------------------------------------------

export default class App extends Component {

  render() {
    return (
    
          <View style={{ flex: 1 }}>
            <AppDrawerNavigator />
          </View>
       
    );
  }
}

const CustomDrawerComponent = (props) => (
  <SafeAreaView style={{ flex: 1 }}>
    <View style={{ height: 150, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }}>
      <Image source={require('./src/component/img/coffee.png')} />
      <Text>Owner</Text>
    </View>
    <ScrollView>
      <DrawerItems {...props} />
    </ScrollView>
  </SafeAreaView>
)
const AppDrawerNavigator = createDrawerNavigator({
  Sale: saleScreen,
  Order: orderScreen,
  Customer: customerScreen,
  Statistic: statisticScreen,
  Management: manScreen,
  // Logout: logoutScreen

}, {
    contentComponent: CustomDrawerComponent,
    // drawerWidth : width,
    contentOptions: {
      activeTintColor: '#29ab87'
    }
  })

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

// const mapStateToProps = ({  }) => {
//   return{

//   }
// }

// const mapDispatchToProps = dispatch => {
//   return {
//     getAllAction: () => dispatch(getAll()),
//   }
// }

// export default
//   connect(
//     mapStateToProps,
//     mapDispatchToProps
//   )(router)

