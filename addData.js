
// //===============================ADD STAFF========================================
import React, { Component } from 'react';
import {
    View,
    FlatList,
    Image,
    Text,
    TouchableHighlight,
    Alert,
    Modal,
    ScrollView,
    TextInput,
    Button
} from 'react-native';
import Firebase from './src/Firebase/firebase'
import * as firebase from 'firebase';

if (!firebase.apps.length) {
    firebase.initializeApp(Firebase.FirebaseConfig);
}

export default class App extends Component {


    constructor(props) {
        super(props);
        this.state = {
            id: '',
            imagePath: '',
            name: '',
            price: '',
            type: '',
        }
    }



    addAccount = () => {

        let {
            id,
            imagePath,
            name,
            price,
            type,
        } = this.state

        firebase.database().ref('coffee/').push({
            id,
            imagePath,
            name,
            price,
            type,
        }).then((data) => {
            //success callback
            console.log('data ', data)
        }).catch((error) => {
            //error callback
            console.log('error ', error)
        })

        this.setState({
            id,
            imagePath,
            name,
            price,
            type,
        });
    }

    render() {
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>

                <View style={coinDetailStyle.containerMain}>
                    <View style={coinDetailStyle.containerInput}>
                        <Text style={coinDetailStyle.textForm} >ID</Text>
                        <TextInput style={coinDetailStyle.textInputEmail}
                            autoCorrect={false}
                            placeholder='id'
                            onChangeText={(text) => { this.setState({ id: text }) }}
                        ></TextInput>
                    </View>
                    <View style={coinDetailStyle.containerInput}>
                        <Text style={coinDetailStyle.textForm} >Username</Text>
                        <TextInput style={coinDetailStyle.textInputEmail}
                            autoCorrect={false}
                            placeholder='name'
                            onChangeText={(text) => { this.setState({ name: text }) }}
                        ></TextInput>
                    </View>
                    <View style={coinDetailStyle.containerInput}>
                        <Text style={coinDetailStyle.textForm}>Password</Text>
                        <TextInput style={coinDetailStyle.textInputPassword}
                            autoCorrect={false}
                            placeholder='price'
                            // secureTextEntry={true}
                            onChangeText={(text) => { this.setState({ price: text }) }}
                        ></TextInput>
                    </View>
                    <View style={coinDetailStyle.containerInput}>
                        <Text style={coinDetailStyle.textForm}>Name</Text>
                        <TextInput style={coinDetailStyle.textInputPassword}
                            autoCorrect={false}
                            placeholder='type'
                            onChangeText={(text) => { this.setState({ type: text }) }}
                        ></TextInput>
                    </View>
                    <View style={coinDetailStyle.containerInput}>
                        <Text style={coinDetailStyle.textForm}>Last Name</Text>
                        <TextInput style={coinDetailStyle.textInputPassword}
                            autoCorrect={false}
                            placeholder='img'
                            onChangeText={(text) => { this.setState({ imagePath: text }) }}
                        ></TextInput>
                    </View>
                    {/* <View style={coinDetailStyle.containerInput}>
                        <Text style={coinDetailStyle.textForm}>License</Text>
                        <TextInput style={coinDetailStyle.textInputPassword}
                            autoCorrect={false}
                            placeholder='License'
                            onChangeText={(text) => { this.setState({ license: text }) }}
                        ></TextInput>
                    </View> */}
                    <Button
                        title="SAVE"
                        onPress={() => {
                            this.addAccount();
                        }}
                    />

                    <Button
                        title="CLEAR"
                        onPress={() => {
                            this.clear();
                        }}
                    />
                </View>

            </View>

        );
    }
}

const coinDetailStyle = {
    containerMain: {
        flexDirection: 'column',
        flex: 1,
        marginTop: 100
    },
    containerInput: {
        flexDirection: 'row',
        marginBottom: 6,
    },
    textForm: {
        fontSize: 18,
        flex: 1,
    },
    textInputEmail: {
        padding: 4,
        fontSize: 18,
        height: 30,
        width: 100,
        borderColor: '#DDDDDD',
        borderWidth: 1,
        flex: 2,
    },
    textInputPassword: {
        padding: 4,
        fontSize: 18,
        height: 30,
        width: 100,
        borderColor: '#DDDDDD',
        borderWidth: 1,
        flex: 2
    },
    buttonLogin: {
        marginTop: 20,
        fontSize: 20,
        alignContent: 'center',
    }

};