/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
// import App from './addData';
// import App from './src/component/addAccount';
// import App from './src/component/appRedux';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
