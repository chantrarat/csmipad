
import { createDrawerNavigator, DrawerItems } from 'react-navigation'
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  Dimensions,
  Image
} from 'react-native';

import { MenuProvider } from 'react-native-popup-menu';
import saleScreen from './src/component/sale'
import orderScreen from './src/component/order'
import customerScreen from './src/component/customer'
import manScreen from './src/component/man'

export default class App extends Component {

  render() {
    return (
        <MenuProvider>
          <View style={{ flex: 1 }}>
            <AppDrawerNavigator />
          </View>
        </MenuProvider>
    );
  }
}

const CustomDrawerComponent = (props) => (
  <SafeAreaView style={{ flex: 1 }}>
    <View style={{ height: 150, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }}>
      <Image source={require('./src/component/img/coffee.png')} />
      <Text>Staff</Text>
    </View>
    <ScrollView>
      <DrawerItems {...props} />
    </ScrollView>
  </SafeAreaView>
)
const AppDrawerNavigator = createDrawerNavigator({
  Sale: saleScreen,
  Order: orderScreen,
  Customer: customerScreen,
}, {
    contentComponent: CustomDrawerComponent,
    // drawerWidth : width,
    contentOptions: {
      activeTintColor: '#29ab87'
    }
  })

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

