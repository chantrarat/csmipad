import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import {
    Icon,
} from 'native-base';
import Owner from './Owner'
import Staff from './Staff'
import Login from './App'
import * as firebase from 'firebase';
import Firebase from './src/Firebase/firebase'

//redux-------------------------------------------------------------------
import { combineReducers } from 'redux'
import reducers from './src/Reducer'
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import { Provider } from 'react-redux'

//redux-------------------------------------------------------------------
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const reducer = combineReducers({ reducers })
const store = createStore(
  reducer,
  composeEnhancer(applyMiddleware(thunk, logger))
  // composeEnhancer(applyMiddleware(thunk))     // กรณีไม่ต้องการให้ log เมื่อ state redux เปลี่ยน
)
if (!firebase.apps.length) {
  firebase.initializeApp(Firebase.FirebaseConfig);
}

export default class LogoutView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      user: 'Owner',
      statusLogin: false,
    }
  }

//   static navigationOptions = {
//     drawerIcon: ({ tintColor }) => (
//       <Icon name="lock" style={{ fontSize: 24, color: tintColor }} />
//     )
//   }
  onClickListener = (viewId) => {
    Alert.alert("Alert", "Button pressed " + viewId);
  }

  login = () => {
    firebase.database().ref('/Account')
      .orderByChild('username')
      .equalTo(this.state.username)
      .on('value', (data) => {

        if (data.val() != null) {
          console.log("DATA", data.val());
          let d = data.val()
          // console.log(Object.values(d));
          // console.log("74", Object.values(d)[0].username);
          let _password = Object.values(d)[0].password
          if (this.state.password === _password) {
            let _rank = Object.values(d)[0].rank
            if (_rank === 'Owner') {
              this.setState({
                statusLogin: true,
                user: 'Owner'
              })
            } else {
              this.setState({
                statusLogin: true,
                user: 'Staff'
              })
            }
          } else {

          }
        } else {
          console.log("xxxx");
        }



      })

    // if (this.state.username === 'Owner') {
    //   this.setState({
    //     statusLogin: true,
    //     user: 'Owner'
    //   })
    // } else {
    //   this.setState({
    //     statusLogin: true,
    //     user: 'Staff'
    //   })
    // }

  }
  // componentDidMount() {


  // }

  showComponent = () => {
    if (this.state.statusLogin) {
      if (this.state.user === 'Owner') {
        return <Owner></Owner>
      } else {
        return <Staff></Staff>
      }
    } else {
      return <Login></Login>
    }
  }

  render() {


    return (
      < Provider store={store} >
        {this.showComponent()}
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DCDCDC',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
  },
  loginButton: {
    backgroundColor: "#00b5ec",
  },
  loginText: {
    color: 'white',
  }
});