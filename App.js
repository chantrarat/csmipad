import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import Owner from './Owner'
import Staff from './Staff'
import Admin from './src/component/addAccount'
import * as firebase from 'firebase';

//redux-------------------------------------------------------------------
import { combineReducers } from 'redux'
import reducers from './src/Reducer'
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import { Provider } from 'react-redux'

//redux-------------------------------------------------------------------
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const reducer = combineReducers({ reducers })
const store = createStore(
  reducer,
  composeEnhancer(applyMiddleware(thunk, logger))
  // composeEnhancer(applyMiddleware(thunk))     // กรณีไม่ต้องการให้ log เมื่อ state redux เปลี่ยน
)
if (!firebase.apps.length) {
  firebase.initializeApp(Firebase.FirebaseConfig);
}

export default class LoginView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      license: '',
      statusLogin: '',
    }
  }

  onClickListener = (viewId) => {
    Alert.alert("Alert", "Button pressed " + viewId);
  }

  login = () => {
    firebase.database().ref('/Account')
      .orderByChild('username')
      .equalTo(this.state.username)
      .on('value', (data) => {

        if (data.val() != null) {
          console.log("DATA", data.val());
          let d = data.val()
          console.log(Object.values(d));
          console.log(typeof Object.keys(d));
          let key = Object.keys(d)[0]
          console.log("74", Object.values(d)[0].username);
          let _password = Object.values(d)[0].password
          if (this.state.password === _password) {
            let _license = Object.values(d)[0].license
            switch (_license) {
              case 'Owner':
                firebase.database().ref().child('/Account/' + key).update(
                  {
                    statusLogin: true,
                  })

                this.setState({
                  statusLogin: true,
                  user: 'Owner'
                })
                break;

              case 'Staff':
                firebase.database().ref().child('/Account/' + key).update(
                  {
                    statusLogin: true,
                  })

                this.setState({
                  statusLogin: true,
                  user: 'Staff'
                })
                break;

              case 'Admin':
                firebase.database().ref().child('/Account/' + key).update(
                  {
                    statusLogin: true,
                  })

                this.setState({
                  statusLogin: true,
                  user: 'Admin'
                })
                break;

              default:
                break;
            }
            // if (_license === 'Owner') {
            //   firebase.database().ref().child('/Account/' + key).update(
            //     {
            //       statusLogin: true,
            //     })

            //   this.setState({
            //     statusLogin: true,
            //     user: 'Owner'
            //   })
            // } 
            // else {
            //   firebase.database().ref().child('/Account/' + key).update(
            //     {
            //       statusLogin: true,
            //     })

            //   this.setState({
            //     statusLogin: true,
            //     user: 'Staff'
            //   })
            // }
          } else {
            console.log(" incorrectpassword")
          }
        } else {
          console.log("null")
        }
      })
  }


  showComponent = () => {
    if (this.state.statusLogin) {
      // if (this.state.user === 'Owner') {
      //   return <Owner></Owner>
      // } else {
      //   return <Staff></Staff>
      // }

      switch (this.state.user) {
        case 'Owner':
          return <Owner></Owner>
          
        case 'Staff':
          return <Staff></Staff>

        case 'Admin':
          return <Admin></Admin>

        default:
          break;
      }

    } else {
      return <View style={styles.container}>
        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{ uri: 'https://png.icons8.com/people' }} />
          <TextInput style={styles.inputs}
            placeholder="Username"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            onChangeText={(username) => this.setState({ username })}

          />
        </View>

        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{ uri: 'https://png.icons8.com/key' }} />
          <TextInput style={styles.inputs}
            placeholder="Password"
            secureTextEntry={true}
            underlineColorAndroid='transparent'
            onChangeText={(password) => this.setState({ password })} />
        </View>

        {/* <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => checkUser('Owner')}> */}
        <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.login()}>
          <Text style={styles.loginText}>Login</Text>

        </TouchableHighlight>

        {/* <TouchableHighlight style={styles.buttonContainer} onPress={() => this.onClickListener('restore_password')}>
          <Text>Forgot your password?</Text>
        </TouchableHighlight>

        <TouchableHighlight style={styles.buttonContainer} onPress={() => this.onClickListener('register')}>
          <Text>Register</Text>
        </TouchableHighlight> */}
      </View>
    }
  }

  render() {


    return (
      < Provider store={store} >
        {this.showComponent()}
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#29ab87',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
  },
  loginButton: {
    backgroundColor: "rgb(61,61,61)",
  },
  loginText: {
    color: 'white',
  }
});