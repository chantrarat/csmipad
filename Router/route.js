import React, { Component } from 'react';
import { connect } from 'react-redux'
import {
    Platform,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { getAll } from '../src/Action/index'

import { createDrawerNavigator } from 'react-navigation'
import { Router, Scene } from 'react-native-router-flux'
import saleScreen from '../src/component/sale'
import orderScreen from '../src/component/order'
import customerScreen from '../src/component/customer'
import manScreen from '../src/component/man'


export class router extends Component {
    render() {
        return (
            <Router>
                <Scene key="root" title="welcome" hideNavBar={true} >
                    <Scene key="tabbar" tabs >
                        <Scene
                            key="tab1"
                            title="SALE"
                            
                        >
                            <Scene
                                key="sale"
                                component={saleScreen}
                                initial
                            />
                        </Scene>

                        <Scene
                            key="tab2"
                            title="ORDER"
                        >
                            <Scene
                                key="order"
                                component={orderScreen}
                            />
                        </Scene>

                        <Scene
                            key="tab3"
                            title="CUSTOMER"
                            
                        >
                            <Scene
                                key="customer"

                                component={customerScreen}
                            />
                        </Scene>

                        <Scene
                            key="tab4"
                            title="MANAGEMENT"
                            
                        >
                            <Scene
                                key="management"

                                component={manScreen}
                            />
                        </Scene>

                    </Scene>
                </Scene>
            </Router>
        );
    }
}

const mapStateToProps = ({  }) => {
    return{
        
    }
  }
  
  const mapDispatchToProps = dispatch => {
    return {
      getAllAction: () => dispatch(getAll()),
    }
  }
  
  export default
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(router)

