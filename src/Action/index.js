

const addItem = payload => {
    return {
        type: 'addItem',
        payload //{ orderItem , type , level ,numShot: shot ,sweet ,milk ,topping ,size ,numPiece: 1}
    }
}

const clearItem = () => {
    return {
        type: 'clearItem'
    }
}

const addBill = payload => {
    return {
        type: 'addBill',
        payload 
    }
}

const updateList = payload => {
    return {
        type: 'updateList',
        payload
    }
}


const setCoffeeData = payload => {
    return {
        type: 'setCoffeeData',
        payload
    }
}

const setNonCoffeeData = payload => {
    return {
        type: 'setNonCoffeeData',
        payload
    }
}
const setBakeryData = payload => {
    return {
        type: 'setBakeryData',
        payload
    }
}

const setTeaData = payload => {
    return {
        type: 'setTeaData',
        payload
    }
}

const setSodaData = payload => {
    return {
        type: 'setSodaData',
        payload
    }
}

const setCocoaData = payload => {
    return {
        type: 'setCocoaData',
        payload
    }
}

const setMilkData = payload => {
    return {
        type: 'setMilkData',
        payload
    }
}

const getAll = ()=> {
    return {
        type: 'getAll',
        payload: {}
    }
}

// this.props.getCoffeeMenuAction()
// this.props.getSodaMenuAction()
// this.props.getTeaMenuAction()
// this.props.getMilkMenuAction()
// this.props.getCocoaMenuAction()
// this.props.getBakeryMenuAction()

const getCoffeeMenu = ()=> {
    return {
        type: 'getAll',
        payload: {}
    }
}

const getSodaMenu = ()=> {
    return {
        type: 'getAll',
        payload: {}
    }
}

const getTeaMenu = ()=> {
    return {
        type: 'getAll',
        payload: {}
    }
}

const getMilkMenu = ()=> {
    return {
        type: 'getAll',
        payload: {}
    }
}

const getCocoaMenu = ()=> {
    return {
        type: 'getAll',
        payload: {}
    }
}

const getBakeryMenu = ()=> {
    return {
        type: 'getAll',
        payload: {}
    }
}

module.exports = {
    getAll,
    addItem,
    addBill,
    setTeaData,
    setCoffeeData,
    setNonCoffeeData,
    setBakeryData,
    setSodaData,
    setCocoaData,
    setMilkData,
    updateList,
    clearItem,
}