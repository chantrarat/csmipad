import React, { Component } from 'react';
import {
  View,
  FlatList,
  Image,
  Text,
  TouchableHighlight,
  Alert,
  Modal,
  ScrollView,
} from 'react-native';

import styles from '../styles'
import { Form } from 'native-base';


export default class App extends Component {
  render() {

    //destructrue
    const {
      bakeryData,

      //function get
      showOptionModal,
      modalOption,
      

    } = this.props

    return (
      <View style={styles.mainBox}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={modalOption}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>

          <View style={{ flex: 1, marginTop: 22, flexDirection: 'row' }}>
            <View style={{ flex: 0.8 }}>

            </View>

            <View style={{ flex: 0.2 }}>
              <Form style={{ marginBottom: 12 }}>
                <TouchableHighlight
                  onPress={() => {
                    showOptionModal(false, -1, "bakery");
                  }}>
                  <View style={{ width: 180, height: 700, backgroundColor: '#29ab87', justifyContent: 'center', marginTop: 25 }} >
                    <Text style={{ fontSize: 30, textAlign: 'center', color: 'white', marginTop: 5 }}>ORDER</Text>
                  </View>
                </TouchableHighlight>
              </Form>
            </View>
          </View>

        </Modal>

        {/* ======================================================================================================================================================= */}



        {/* ________________________________________ ITEM LIST________________________________________ */}
        <FlatList
          data={bakeryData}
          renderItem={({ item }) => (

            <View style={{ flex: 0.16, flexDirection: 'column', margin: 1 }}>
              <TouchableHighlight onPress={() => { showOptionModal(true, item.id, "bakery"); }}>
                <Image style={styles.imageThumbnail} source={{ uri: item.src }} />
              </TouchableHighlight>
              <Text style={{ textAlign: 'center', fontSize: 20 }}> {item.name}</Text>
              <View style={styles.mainBox}>
              </View>
            </View>
          )}
          //Setting the number of column
          numColumns={6}
          keyExtractor={(item, index) => index}
        />
        {/* ________________________________________END ITEM LIST________________________________________ */}

      </View>
    );
  }
}