import React, { Component } from 'react';
import {
  View,
  FlatList,
  Image,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  Alert,
  Modal,
  ScrollView,
} from 'react-native';
import styles from '../styles'
import { Form } from 'native-base';



export default class App extends Component {
  render() {

    //destructrue
    const {
      coffeeData,
      shot,

      //function get
      showOptionModal,
      modalOption,
      addShot,
      reduceShot,
      getType,
      getLevel,
      getSweet,
      getMilk,
      getTopping,
      getSize,

    } = this.props

    return (
      <View style={styles.mainBox}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={modalOption}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>

          <View style={{ flex: 1, marginTop: 22, flexDirection: 'row' }}>
            <View style={{ flex: 0.8 }}>
              <ScrollView>
              <Text style={{ fontSize: 20, marginTop: 10, textAlign: 'center' }}>________________________________________TYPE________________________________________</Text>
                <View style={{ flexDirection: 'row', justifyContent: "center", marginTop: 10 }}>
                  <TouchableOpacity onPress={() => getType('hot')}>
                    <View style={{ width: 190, height: 100, backgroundColor: '#b90e0a', flexDirection: 'row', justifyContent: 'center' }} >
                      <Text style={{ fontSize: 80, textAlign: 'center', marginTop: 5, color: 'white' }}>HOT</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => getType('ice')}>
                    <View style={{ width: 190, height: 100, backgroundColor: '#0288d1', marginLeft: 5, flexDirection: 'row', justifyContent: 'center' }}>
                      <Text style={{ fontSize: 80, textAlign: 'center', marginTop: 5, color: 'white' }}>ICE</Text>
                    </View>
                  </TouchableOpacity>
                  
                </View>
                
              {/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */}

              <Text style={{ fontSize: 20, marginTop: 10, textAlign: 'center' }}>________________________________________LEVEL________________________________________</Text>
              <View style={{ flexDirection: 'row', justifyContent: "center", marginTop: 10 }}>
                <TouchableOpacity onPress={() => getLevel('dark')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#5b4b41', flexDirection: 'row' }} >
                    <Text style={{ fontSize: 80, textAlign: 'center', marginLeft: 15, color: 'white' }}>1</Text>
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'white' }}>DARK</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => getLevel('black')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#684c38', marginLeft: 5, flexDirection: 'row' }}>
                    <Text style={{ fontSize: 80, textAlign: 'center', marginLeft: 15, color: 'white' }}>2</Text>
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'white' }}>BLACK</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => getLevel('brown')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#c08457', marginLeft: 5, flexDirection: 'row' }}>
                    <Text style={{ fontSize: 80, textAlign: 'center', marginLeft: 15, color: 'white' }}>3</Text>
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'white' }}>BROWN</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => getLevel('light')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#e4c0a8', marginLeft: 5, flexDirection: 'row' }}>
                    <Text style={{ fontSize: 80, textAlign: 'center', marginLeft: 15, color: 'white' }}>4</Text>
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'white' }}>LIGHT</Text>
                  </View>
                </TouchableOpacity>
              </View>
              {/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */}
              <Text style={{ fontSize: 20, marginTop: 10, textAlign: 'center' }}>________________________________________SHOT________________________________________</Text>
              <View style={{ flexDirection: 'row', justifyContent: "center", marginTop: 10 }}>
                <TouchableOpacity onPress={() => reduceShot(shot)}>
                  <Text style={{ fontSize: 50 }}>-</Text>
                </TouchableOpacity>
                  <Text style={{ fontSize: 50, marginLeft: 20 }}>{shot}</Text>
                <TouchableOpacity onPress={() => addShot(shot)}>
                  <Text style={{ fontSize: 50, marginLeft: 30 }}>+</Text>
                </TouchableOpacity>
              </View>
              {/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */}
              <Text style={{ fontSize: 20, marginTop: 10, textAlign: 'center' }}>________________________________________SWEET________________________________________</Text>
              <View style={{ flexDirection: 'row', justifyContent: "center", marginTop: 10 }}>

                <TouchableOpacity onPress={() => getSweet('no sugar')}>
                  <View style={{ width: 190, height: 70, backgroundColor: '#ffc4cd', marginLeft: 5, flexDirection: 'row', justifyContent: 'center' }} >
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'white' }}>NO SUGAR</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => getSweet('less sugar')}>
                  <View style={{ width: 190, height: 70, backgroundColor: '#ffb6c1', marginLeft: 5, flexDirection: 'row', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'white' }}>LESS SUGAR</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => getSweet('normal')}>
                  <View style={{ width: 190, height: 70, backgroundColor: '#ff9dab', marginLeft: 5, flexDirection: 'row', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'white' }}>NORMAL</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => getSweet('sweet')}>
                  <View style={{ width: 190, height: 70, backgroundColor: '#ff8396', marginLeft: 5, flexDirection: 'row', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'white' }}>SWEET</Text>
                  </View>
                </TouchableOpacity>
              </View>
              {/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */}
              <Text style={{ fontSize: 20, marginTop: 10, textAlign: 'center' }}>________________________________________MILK________________________________________</Text>
              <View style={{ flexDirection: 'row', justifyContent: "center", marginTop: 10 }}>

                <TouchableOpacity onPress={() => getMilk('no milk')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#F0FFFF', marginLeft: 5, flexDirection: 'row' }} >
                    
                    <Text style={{ fontSize: 26, textAlign: 'center', marginTop: 15, color: 'black' }}>NO MILK</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => getMilk('milk')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#BBFFFF', marginLeft: 5, flexDirection: 'row' }}>
                    
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'black', marginLeft: 15 }}>MILK</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => getMilk('soy')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#97FFFF', marginLeft: 5, flexDirection: 'row' }}>
                   
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'black', marginLeft: 5 }}>SOY</Text>
                  </View>
                </TouchableOpacity>

              </View>
              {/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */}
              <Text style={{ fontSize: 20, marginTop: 10, textAlign: 'center' }}>________________________________________TOPPING________________________________________</Text>
              <View style={{ flexDirection: 'row', justifyContent: "center", marginTop: 10 }}>

                <TouchableOpacity onPress={() => getTopping('syrup')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#ffffea', marginLeft: 5, flexDirection: 'row' }} >
                   
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'black' }}>SYRUP</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => getTopping('honey')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#ffff9c', marginLeft: 5, flexDirection: 'row' }}>
                    
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'black' }}>HONEY</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => getTopping('whip cream')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#ffff4e', marginLeft: 5, flexDirection: 'row' }}>
                    
                    <View style={{ flexDirection: 'column' }}>
                      <Text style={{ fontSize: 26, textAlign: 'center', marginTop: 15, color: 'black', marginLeft: 5 }}>WHIP</Text>
                      <Text style={{ fontSize: 26, textAlign: 'center', marginTop: 2, color: 'black', marginLeft: 8 }}>CREAM</Text>
                    </View>
                  </View>
                </TouchableOpacity>

              </View>
              {/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */}
              <Text style={{ fontSize: 20, marginTop: 10, textAlign: 'center', marginTop: 10 }}>________________________________________SIZE________________________________________</Text>

              <View style={{ flexDirection: 'row', justifyContent: "center", marginBottom: 20 }}>

                <TouchableOpacity onPress={() => getSize('S')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#5b4b41', marginLeft: 5, flexDirection: 'row' }} >
                    
                    <Text style={{ fontSize: 70, textAlign: 'center', marginTop: 10, color: 'white', marginLeft: 15 }}>S</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => getSize('M')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#684c38', marginLeft: 5, flexDirection: 'row' }}>
                    
                    <Text style={{ fontSize: 70, textAlign: 'center', marginTop: 10, color: 'white', marginLeft: 15 }}>M</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => getSize('L')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#c08457', marginLeft: 5, flexDirection: 'row' }}>
                    
                    <Text style={{ fontSize: 70, textAlign: 'center', marginTop: 10, color: 'white', marginLeft: 15 }}>L</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => getSize('XL')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#c08457', marginLeft: 5, flexDirection: 'row' }}>
                    <Text style={{ fontSize: 70, textAlign: 'center', marginTop: 10, color: 'white', marginLeft: 10 }}>XL</Text>
                  </View>
                </TouchableOpacity>
              </View>
              </ScrollView>
            </View>
            
            <View style={{ flex: 0.2 }}>
              <Form style={{ marginBottom: 12 }}>

              <TouchableOpacity
                  onPress={() => {
                    showOptionModal(!modalOption, -1, "cancle");
                  }}>
                  <View style={{ width: 180, height: 150, backgroundColor: '#b90e0a', justifyContent: 'center', marginTop: 25 }} >
                    <Text style={{ fontSize: 30, textAlign: 'center', color: 'white', marginTop: 5 }}>CANCLE</Text>
                  </View>
                </TouchableOpacity>


                <TouchableOpacity
                  onPress={() => {
                    showOptionModal(!modalOption, -1, "coffee");
                  }}>
                  <View style={{ width: 180, height: 550, backgroundColor: '#29ab87', justifyContent: 'center', marginTop: 5 }} >
                    <Text style={{ fontSize: 30, textAlign: 'center', color: 'white', marginTop: 5 }}>ORDER</Text>
                  </View>
                </TouchableOpacity>

              </Form>
            </View>
          </View>

        </Modal>

        {/* ======================================================================================================================================================= */}

        {/* ________________________________________ COFFEE DATA LIST________________________________________ */}
        <FlatList
          data={coffeeData}
          renderItem={({ item }) => (

            <View style={{ flex: 0.16, flexDirection: 'column', margin: 1 }}>
              <TouchableHighlight onPress={() => { showOptionModal(true, item.id, "coffee"); }}>
                <Image style={styles.imageThumbnail} source={{ uri: item.src }} />
              </TouchableHighlight>
              <Text style={{ textAlign: 'center', fontSize: 20 }}> {item.name}</Text>
              <View style={styles.main}>
              </View>
            </View>
          )}
          //Setting the number of column
          numColumns={6}
          keyExtractor={(item, index) => index}
        />
        {/* ________________________________________END COFFEE DATA LIST________________________________________ */}

      </View>
    ); 
  }
}
