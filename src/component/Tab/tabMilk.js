import React, {Component} from 'react';
import {
  View,
  FlatList,
  Image,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  Alert,
  Modal,
  ScrollView, 
} from 'react-native';

import styles from '../styles'
import { Form } from 'native-base';


export default class App extends Component{
  render() {

    //destructrue
    const { 
      milkData,
  
    //function get
      showOptionModal,
      modalOption,
      getType,
      getSweet,
      getSize,
       
      } = this.props

    return (
      <View style={styles.mainBox}>
       <Modal
          animationType="slide"
          transparent={false}
          visible={modalOption}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>

          <View style={{ flex: 1, marginTop: 22, flexDirection: 'row' }}>
            <View style={{ flex: 0.8 }}>
            <ScrollView>
              <Text style={{ fontSize: 20, marginTop: 10, textAlign: 'center' }}>________________________________________TYPE________________________________________</Text>
              <View style={{ flexDirection: 'row', justifyContent: "center", marginTop: 10 }}>
                <TouchableHighlight onPress={() => getType('hot')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#b90e0a', flexDirection: 'row', justifyContent: 'center' }} >
                    <Text style={{ fontSize: 80, textAlign: 'center', marginTop: 5, color: 'white' }}>HOT</Text>
                  </View>
                </TouchableHighlight>

                <TouchableHighlight onPress={() => getType('ice')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#0288d1', marginLeft: 5, flexDirection: 'row', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 80, textAlign: 'center', marginTop: 5, color: 'white' }}>ICE</Text>
                  </View>
                </TouchableHighlight>
              </View>

              {/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */}

              <Text style={{ fontSize: 20, marginTop: 10, textAlign: 'center' }}>________________________________________SWEET________________________________________</Text>
              <View style={{ flexDirection: 'row', justifyContent: "center", marginTop: 10 }}>

                <TouchableHighlight onPress={() => getSweet('no sugar')}>
                  <View style={{ width: 190, height: 70, backgroundColor: '#ffc4cd', marginLeft: 5, flexDirection: 'row', justifyContent: 'center' }} >
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'white' }}>NO SUGAR</Text>
                  </View>
                </TouchableHighlight>

                <TouchableHighlight onPress={() => getSweet('less sugar')}>
                  <View style={{ width: 190, height: 70, backgroundColor: '#ffb6c1', marginLeft: 5, flexDirection: 'row', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'white' }}>LESS SUGAR</Text>
                  </View>
                </TouchableHighlight>

                <TouchableHighlight onPress={() => getSweet('normal')}>
                  <View style={{ width: 190, height: 70, backgroundColor: '#ff9dab', marginLeft: 5, flexDirection: 'row', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'white' }}>NORMAL</Text>
                  </View>
                </TouchableHighlight>

                <TouchableHighlight onPress={() => getSweet('sweet')}>
                  <View style={{ width: 190, height: 70, backgroundColor: '#ff8396', marginLeft: 5, flexDirection: 'row', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 15, color: 'white' }}>SWEET</Text>
                  </View>
                </TouchableHighlight>
              </View>
              {/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */}

              <Text style={{ fontSize: 20, marginTop: 10, textAlign: 'center', marginTop: 10 }}>________________________________________SIZE________________________________________</Text>

              <View style={{ flexDirection: 'row', justifyContent: "center",marginBottom: 20 }}>

                <TouchableHighlight onPress={() => getSize('S')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#5b4b41', marginLeft: 5, flexDirection: 'row' }} >
                    <Text style={{ fontSize: 70, textAlign: 'center', marginTop: 10, color: 'white', marginLeft: 15 }}>S</Text>
                  </View>
                </TouchableHighlight>

                <TouchableHighlight onPress={() => getSize('M')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#684c38', marginLeft: 5, flexDirection: 'row' }}>
                    <Text style={{ fontSize: 70, textAlign: 'center', marginTop: 10, color: 'white', marginLeft: 15 }}>M</Text>
                  </View>
                </TouchableHighlight>

                <TouchableHighlight onPress={() => getSize('L')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#c08457', marginLeft: 5, flexDirection: 'row' }}>
                    <Text style={{ fontSize: 70, textAlign: 'center', marginTop: 10, color: 'white', marginLeft: 15 }}>L</Text>
                  </View>
                </TouchableHighlight>

                <TouchableHighlight onPress={() => getSize('XL')}>
                  <View style={{ width: 190, height: 100, backgroundColor: '#c08457', marginLeft: 5, flexDirection: 'row' }}>
                    <Text style={{ fontSize: 70, textAlign: 'center', marginTop: 10, color: 'white', marginLeft: 10 }}>XL</Text>
                  </View>
                </TouchableHighlight>
              </View>
              </ScrollView>
            </View>
            <View style={{ flex: 0.2 }}>
              <Form style={{ marginBottom: 12 }}>

              <TouchableOpacity
                  onPress={() => {
                    showOptionModal(!modalOption, -1, "cancle");
                  }}>
                  <View style={{ width: 180, height: 150, backgroundColor: '#b90e0a', justifyContent: 'center', marginTop: 25 }} >
                    <Text style={{ fontSize: 30, textAlign: 'center', color: 'white', marginTop: 5 }}>CANCLE</Text>
                  </View>
                </TouchableOpacity>


                <TouchableOpacity
                  onPress={() => {
                    showOptionModal(!modalOption, -1, "milk");
                  }}>
                  <View style={{ width: 180, height: 550, backgroundColor: '#29ab87', justifyContent: 'center', marginTop: 5 }} >
                    <Text style={{ fontSize: 30, textAlign: 'center', color: 'white', marginTop: 5 }}>ORDER</Text>
                  </View>
                </TouchableOpacity>

              </Form>
            </View>
          </View>

        </Modal>

      {/* ________________________________________ ITEM LIST________________________________________ */}
      <FlatList
          data={milkData}
          renderItem={({ item }) => (

            <View style={{ flex: 0.16, flexDirection: 'column', margin: 1 }}>
              <TouchableHighlight onPress={() => { showOptionModal(true,item.id, "milk"); }}>
                <Image style={styles.imageThumbnail} source={{ uri: item.src }} />
              </TouchableHighlight>
              <Text style={{ textAlign: 'center', fontSize: 20 }}> {item.name}</Text>
              <View style={styles.mainBox}>
              </View>
            </View>
          )}
          //Setting the number of column
          numColumns={6}
          keyExtractor={(item, index) => index}
        />
 {/* ________________________________________END ITEM LIST________________________________________ */}
        
      </View>
    );
  }
}