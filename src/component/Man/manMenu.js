
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  CheckBox,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  Button as B
} from 'react-native';
import {
  Form,
  Icon,
  Tab,
  Tabs,
  TabHeading,
  Header,
  Left,
  Right,
  Content,
  Container,
  Picker,
  Item,
} from 'native-base';

import styles from '../styles';
import { Col, Row, Grid } from "react-native-easy-grid";
import { TextInput, Button, Searchbar } from 'react-native-paper';

import * as firebase from 'firebase';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from "rn-fetch-blob";
const Blob = RNFetchBlob.polyfill.Blob;
const fs = RNFetchBlob.fs;
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = Blob;

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 'first',
      type: undefined,
      typeMenu: undefined,
      menuData: [],
      name: '',
      price: '',
      firstQuery: '',
      data: ['name', 'name'],
      photo: null,
      coffeeMenu: [],
      teaMenu: [],
      sodaMenu: [],
      milkMenu: [],
      cocoaMenu: [],
      bakeryMenu: [],
      selectTypeMenu: [],
      keyMenu: '',

    }
  }

  componentDidMount() {
    firebase.database().ref('/').on('value', (data) => {
      let c = data.toJSON().coffee
      let waitC = Object.keys(c).length
      console.log("wait", waitC);
      let i = 1
      let coffee = []
      Object.keys(c).forEach((key, index) => {
        coffee.push({ id: i++, key: key, name: c[key].name, price: c[key].price, type: c[key].type, img: c[key].imagePath })
        if (waitC == index + 1) {
          this.setState({
            coffeeMenu: coffee
          })
        }
      })

      let nc = data.toJSON().nonCoffee
      let waitNc = Object.keys(nc).length
      console.log("wait", waitNc);
      let j = 1

      let tea = []
      let soda = []
      let milk = []
      let cocoa = []

      Object.keys(nc).forEach((key, index) => {
        switch (nc[key].type) {
          case "tea":
            tea.push({ id: j++, key: key, name: nc[key].name, price: nc[key].price, type: nc[key].type, img: nc[key].imagePath })
            break;

          case "soda":
            soda.push({ id: j++, key: key, name: nc[key].name, price: nc[key].price, type: nc[key].type, img: nc[key].imagePath })
            break;

          case "milk":
            milk.push({ id: j++, key: key, name: nc[key].name, price: nc[key].price, type: nc[key].type, img: nc[key].imagePath })
            break;

          case "cocoa":
            cocoa.push({ id: j++, key: key, name: nc[key].name, price: nc[key].price, type: nc[key].type, img: nc[key].imagePath })
            break;

          default:
            break;
        }
        if (waitNc == index + 1) {
          this.setState({
            teaMenu: tea,
            sodaMenu: soda,
            milkMenu: milk,
            cocoaMenu: cocoa,
          })
        }
      })

      let b = data.toJSON().bakery
      let waitB = Object.keys(b).length
      console.log("wait", waitB);
      let k = 1
      let bakery = []
      Object.keys(b).forEach((key, index) => {
        bakery.push({ id: k++,key: key, name: b[key].name, price: b[key].price, type: b[key].type, img: b[key].imagePath })
        if (waitB == index + 1) {
          this.setState({
            bakeryMenu: bakery
          })
        }
      })
    })
  }

  openImage = () => {
    const options = {
      title: 'Select Picture',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        console.log(source);


        this.setState({
          photo: source,
        });
      }
    });
  }
  uploadImage(uri, mime = 'image/jpg') {
    return new Promise((resolve, reject) => {
      const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri
      let uploadBlob = null
      let fileName = Date.now()
      const imageRef = firebase.storage().ref('images').child(`${fileName}.jpg`)

      fs.readFile(uploadUri, 'base64')
        .then((data) => {
          return Blob.build(data, { type: `${mime};BASE64` })
        })
        .then((blob) => {
          uploadBlob = blob
          return imageRef.put(blob, { contentType: mime })
        })
        .then(() => {
          uploadBlob.close()
          return imageRef.getDownloadURL()
        })
        .then((url) => {
          resolve(url)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
  static navigationOptions = {
    drawerIcon: ({ tintColor }) => (
      <Icon name="settings" style={{ fontSize: 24, color: tintColor }} />
    )
  }
  onValueChange2(value) {
    this.setState({
      type: value
    });
  }
  onTypeMenuChange(value) {
    switch (value) {
      case "coffee":
        this.setState({
          selectTypeMenu: this.state.coffeeMenu
        })
        break;

      case "tea":
        this.setState({
          selectTypeMenu: this.state.teaMenu
        })
        break;

      case "soda":
        this.setState({
          selectTypeMenu: this.state.sodaMenu
        })
        break;

      case "milk":
        this.setState({
          selectTypeMenu: this.state.milkMenu
        })
        break;

      case "cocoa":
        this.setState({
          selectTypeMenu: this.state.cocoaMenu
        })
        break;

      case "bakery":
        this.setState({
          selectTypeMenu: this.state.bakeryMenu
        })
        break;

      default:
        break;
    }


    this.setState({
      typeMenu: value
    });
  }
  clear() {
    this.setState({
      name: '',
      price: '',
      type: undefined,
      photo: null,
    })
  }

  onSave = () => {
    console.log("Save");
    let { photo, name, price, type } = this.state

    this.uploadImage(photo.uri).then(url => {
      console.log("URL Image: ", url);
      // บันทึกข้อม database ที่อยู่ในตัวแปร  url
      if (type === 'coffee' || type === 'bakery') {
        firebase.database().ref(`${type}`).push({
          name: name,
          price: parseInt(price),
          type: type,
          imagePath: url

        })
      } else {
        firebase.database().ref(`nonCoffee`).push({
          name: name,
          price: parseInt(price),
          type: type,
          imagePath: url
        })
      }

      alert('uploaded');
    })
      .catch(error => {
        console.log(error)
        alert('uploaded')

      })
      this.setState({
        type: undefined,
        name: '',
        price: '',
        photo: null,
      })

  }

  deleteItem = (index, key, type) => {

    // firebase.database().ref('/' + type + '/' + key).remove();
    switch (type) {
      case "coffee":
        firebase.database().ref('coffee/' + key).remove();
        break;

      case "tea":
        firebase.database().ref('nonCoffee/' + key).remove();
        break;

      case "soda":
        firebase.database().ref('nonCoffee/' + key).remove();
        break;

      case "milk":
        firebase.database().ref('nonCoffee/' + key).remove();
        break;

      case "cocoa":
        firebase.database().ref('nonCoffee/' + key).remove();
        break;

      case "bakery":
        firebase.database().ref('nonCoffee/' + key).remove();
        break;

      default:
        break;
    }
    this.setState({
      typeMenu: undefined,
    })

  }

  edit = (index, key, type) => {
    let {
      coffeeMenu,
      teaMenu,
      sodaMenu,
      milkMenu,
      cocoaMenu,
      bakeryMenu,
    } = this.state
    console.log(index, key, type);


    switch (type) {
      case "coffee":
        let coffeeEdit = coffeeMenu.find(coffeeMenu => coffeeMenu.key === key)
        this.setState({
          name: coffeeEdit.name,
          type: coffeeEdit.type,
          price: String(coffeeEdit.price),
          photo: { uri: coffeeEdit.img },
          keyMenu: key

        })
        break;

      case "tea":
        let teaEdit = teaMenu.find(teaMenu => teaMenu.key === key)
        this.setState({
          name: teaEdit.name,
          type: teaEdit.type,
          price: String(teaEdit.price),
          photo: { uri: teaEdit.img },
          keyMenu: key

        })
        break;

      case "soda":
        let sodaEdit = sodaMenu.find(sodaMenu => sodaMenu.key === key)
        this.setState({
          name: sodaEdit.name,
          type: sodaEdit.type,
          price: String(sodaEdit.price),
          photo: { uri: sodaEdit.img },
          keyMenu: key

        })
        break;

      case "milk":
        let milkEdit = milkMenu.find(milkMenu => milkMenu.key === key)
        this.setState({
          name: milkEdit.name,
          type: milkEdit.type,
          price: String(milkEdit.price),
          photo: { uri: milkEdit.img },
          keyMenu: key

        })
        break;

      case "cocoa":
        let cocoaEdit = cocoaMenu.find(cocoaMenu => cocoaMenu.key === key)
        this.setState({
          name: cocoaEdit.name,
          type: cocoaEdit.type,
          price: String(cocoaEdit.price),
          photo: { uri: cocoaEdit.img },
          keyMenu: key

        })
        break;

      case "bakery":
        let bakeryEdit = bakeryMenu.find(bakeryMenu => bakeryMenu.key === key)
        this.setState({
          name: bakeryEdit.name,
          type: bakeryEdit.type,
          price: String(bakeryEdit.price),
          photo: { uri: bakeryEdit.img },
          keyMenu: key

        })
        break;

      default:
        break;
    }



  }



  render() {
    const { firstQuery, photo } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <Content contentContainerStyle={{ flex: 1 }}>
          <Grid style={{ backgroundColor: "#FFFFFF" }}>
            <Col >
              <Col style={{ justifyContent: 'center' }}>
                <Col style={{ marginLeft: 20, marginRight: 20, marginTop: 30 }}>
                  <ScrollView>
                    <View style={{ flex: 1 }}>


                      <TouchableOpacity onPress={this.openImage}>
                        <Image style={{ justifyContent: "center", width: 200, height: 200, marginLeft: 130 }} source={photo == null ? require('../img/image.png') : { uri: photo.uri }} />
                      </TouchableOpacity>


                      <TextInput
                        style={{ marginBottom: 20, marginTop: 20 }}
                        label='Name'
                        mode='outlined'
                        value={this.state.name}
                        onChangeText={name => this.setState({ name })}
                      />
                      <TextInput
                        style={{ marginBottom: 20 }}
                        label='Price'
                        mode='outlined'
                        value={this.state.price}
                        onChangeText={price => this.setState({ price })}
                      />

                      <Item picker >
                        <Picker
                          mode='dropdown'
                          iosIcon={<Icon name="arrow-down" />}
                          style={{ width: undefined, marginTop: 10, marginBottom: 10 }}
                          placeholder="Select type"
                          textStyle={{ color: "black" }}
                          itemStyle={{
                            backgroundColor: "white",
                            marginLeft: 0,
                            paddingLeft: 10
                          }}
                          itemTextStyle={{ color: 'black' }}
                          onChangeText={(type) => this.setState({ type })}
                          selectedValue={this.state.type}
                          onValueChange={this.onValueChange2.bind(this)}
                        >

                          <Picker.Item label="Coffee" value="coffee" />
                          <Picker.Item label="Tea" value="tea" />
                          <Picker.Item label="Soda" value="soda" />
                          <Picker.Item label="Cocoa" value="cocoa" />
                          <Picker.Item label="Milk" value="milk" />
                          <Picker.Item label="Bakery" value="bakery" />
                          {/* <Picker.Item label="Others" value="others" /> */}
                        </Picker>
                      </Item>

                      <Row>
                        <Col>
                          <Button

                            mode="outlined"
                            onPress={() => this.clear()}
                            color="#e74c3c"
                            style={{ marginTop: 50, marginLeft: 20, marginRight: 20 }}
                          >
                            Clear
                    </Button>
                        </Col>
                        <Col>
                          <Button

                            mode="outlined" onPress={this.onSave}
                            color="#2ecc71"
                            style={{ marginTop: 50, marginLeft: 20, marginRight: 20 }}
                          >
                            Save
                    </Button>
                        </Col>
                      </Row>
                    </View>
                  </ScrollView>

                </Col>
              </Col>
            </Col>

            {/* Listmenu */}
            <Col style={{ backgroundColor: '#BEBEBE' }}>
              {/* <Searchbar
                style={{ marginLeft: 30, marginRight: 30, marginTop: 30 }}
                placeholder="Search"
                onChangeText={query => { this.setState({ firstQuery: query }); }}
                value={firstQuery}
              /> */}



              <Item picker style={{ backgroundColor: 'white' }}>
                <Picker
                  mode='dropdown'
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined, marginTop: 10, marginBottom: 10 }}
                  placeholder="Select type"
                  textStyle={{ color: "black" }}
                  itemStyle={{
                    backgroundColor: "white",
                    marginLeft: 0,
                    paddingLeft: 10
                  }}
                  itemTextStyle={{ color: 'black' }}
                  onChangeText={(type) => this.setState({ type })}
                  selectedValue={this.state.typeMenu}
                  onValueChange={this.onTypeMenuChange.bind(this)}
                >

                  <Picker.Item label="Coffee" value="coffee" />
                  <Picker.Item label="Tea" value="tea" />
                  <Picker.Item label="Soda" value="soda" />
                  <Picker.Item label="Cocoa" value="cocoa" />
                  <Picker.Item label="Milk" value="milk" />
                  <Picker.Item label="Bakery" value="bakery" />
                  {/* <Picker.Item label="Others" value="others" /> */}
                </Picker>
              </Item>

              <ScrollView keyExtractor={(item, index) => index} style={{ marginLeft: 30, marginRight: 30 }}>
                {

                  this.state.selectTypeMenu.map((item, index) => (

                    <View style={styles.rowOrderView} key={index}>

                      <Text style={styles.textItemOrder}>{item.name}</Text>
                      <View style={styles.itemInOrder}>
                        {
                          console.log(item)

                        }
                        <B
                          onPress={() => this.edit(index, item.key, item.type)}
                          title="Edit"
                        />
                        <TouchableHighlight onPress={() => this.deleteItem(index, item.key, item.type)}>
                          <Icon style={styles.iconTrash} name="ios-trash" />
                        </TouchableHighlight>
                      </View>

                    </View>


                  ))
                }
              </ScrollView>
            </Col>

          </Grid>
        </Content>

      </View>
    );
  }
}

/////////////////////////////////

/*
import React from "react";
import { View, Text, Image, Button, Platform } from "react-native";
import ImagePicker from "react-native-image-picker";
import RNFetchBlob from "rn-fetch-blob";
const Blob = RNFetchBlob.polyfill.Blob;
const fs = RNFetchBlob.fs;
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = Blob;

const createFormData = (photo, body) => {
  const data = new FormData();

  data.append("photo", {
    name: photo.fileName,
    type: photo.type,
    uri:
      Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
  });

  Object.keys(body).forEach(key => {
    data.append(key, body[key]);
  });

  return data;
};
export default class App extends React.Component {
  state = {
    photo: null
  };

  handleChoosePhoto = () => {
    // const options = {
    //   noData: true
    // };
    const options = {
      title: "Select Avatar",
      customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };

    ImagePicker.showImagePicker(options, response => {
      if (response.uri) {
        this.setState({ photo: response });
      }
      this.uploadImage(response.uri)
        .then(url => {
          alert("uploaded");
          this.setState({ image_uri: url });
        })
        .catch(error => console.log(error));
    });
  };

  // handleUploadPhoto = () => {
  //   fetch("http://localhost:3000/api/upload", {
  //     method: "POST",
  //     body: createFormData(this.state.photo, { userId: "123" })
  //   })
  //     .then(response => response.json())
  //     .then(response => {
  //       console.log("upload succes", response);
  //       alert("Upload success!");
  //       this.setState({ photo: null });
  //     })
  //     .catch(error => {
  //       console.log("upload error", error);
  //       alert("Upload failed!");
  //     });
  // };


  uploadImage(uri, mime = "application/octet-stream") {
    return new Promise((resolve, reject) => {
      const uploadUri =
        Platform.OS === "ios" ? uri.replace("file://", "") : uri;
      let uploadBlob = null;

      // const imageRef = FirebaseClient.storage()
      //   .ref("images")
      //   .child("image_001");

      fs.readFile(uploadUri, "base64")
        .then(data => {
          return Blob.build(data, { type: `${mime};BASE64` });
        })
        .then(blob => {
          uploadBlob = blob;
          console.log(blob);
          // return imageRef.put(blob, { contentType: mime });
        })
        .then(() => {
          uploadBlob.close();
          // return imageRef.getDownloadURL();
        })
        .then(url => {
          resolve(url);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
  render() {
    const { photo } = this.state;
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        {photo && (
          <React.Fragment>
            <Image
              source={{ uri: photo.uri }}
              style={{ width: 300, height: 300 }}
            />
            <Button title="Upload" onPress={this.handleUploadPhoto} />
          </React.Fragment>
        )}
        <Button title="Choose Photo" onPress={this.handleChoosePhoto} />
      </View>
    );
  }
}
*/