import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  Button as B,
  TouchableHighlight
} from 'react-native';
import {
  Icon,
  Header,
  Left,
  Content,
  CheckBox,
  Card,
  CardItem,
  List, ListItem,
} from 'native-base';

import styles from '../styles';
import { Col, Row, Grid } from "react-native-easy-grid";
import { TextInput, Button, Searchbar } from 'react-native-paper';
import Firebase from '../../Firebase/firebase'
import * as firebase from 'firebase';

if (!firebase.apps.length) {
  firebase.initializeApp(Firebase.FirebaseConfig);
}

const datas = [
  'Simon Mignolet',
  'Nathaniel Clyne',
  'Dejan Lovren',
  'Mama Sakho',
  'Alberto Moreno',
  'Emre Can',
  'Joe Allen',
  'Phil Coutinho',
  'Simon Mignolet',
  'Nathaniel Clyne',
  'Dejan Lovren',
  'Mama Sakho',
];



export default class saleScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      basic: true,
      listViewData: datas,

      men: false,
      women: false,
      firstQuery: '',
      firstName: '',
      lastName: '',
      nickName: '',
      address: '',
      phone: '',
      uid: '-LbcLhMM8so9uBKKCzbl',
      staff: [],
      keyStaff: ''

    };
  }
  clear() {
    this.setState({
      firstName: '',
      lastName: '',
      nickName: '',
      address: '',
      phone: '',
      men: false,
      women: false,
    })
  }

  static navigationOptions = {
    drawerIcon: ({ tintColor }) => (
      <Icon name="person" style={{ fontSize: 24, color: tintColor }} />
    )
  }
  menPressed() {
    this.setState({ men: true, women: false })
  }
  womenPressed() {
    this.setState({ men: false, women: true })
  }

  componentDidMount() {
    firebase.database().ref('/Account' + '/' + this.state.uid).on('value', (data) => {
      let s = data.toJSON().Staff
      let waitS = Object.keys(s).length
      let i = 1
      let staffList = []
      Object.keys(s).forEach((key, index) => {
        staffList.push({ 
          key: key, 
          id: i++, 
          firstName: s[key].firstName, 
          lastName: s[key].lastName,
          nickName: s[key].nickName,
          address: s[key].address, 
          phone: s[key].phone, 
          men: s[key].men, 
          women: s[key].women 
        })
        if (waitS == index + 1) {
          this.setState({
            staff: staffList
          })
        }
      })
    })
  }
  deleteItem = (index, key) => {
    console.log("STAFF", this.state.staff)
    console.log("INDEX", index)
    console.log("KEY", key)

    firebase.database().ref('/Account' + '/' + this.state.uid + '/' + 'Staff/' + key).remove();


  }

  save = (k) => {

    let {
      firstName,
      lastName,
      nickName,
      address,
      phone,
      men,
      women,
      keyStaff
    } = this.state

    if (keyStaff === '') {
      firebase.database().ref('/Account' + '/' + this.state.uid + '/Staff').push({
        firstName,
        lastName,
        nickName,
        address,
        phone,
        men,
        women,
      }).then((data) => {
        //success callback
        console.log('data ', data)
      }).catch((error) => {
        //error callback
        console.log('error ', error)
      })
    } else {
      firebase.database().ref('/Account'+'/'+ this.state.uid +'/Staff' + '/' + keyStaff).update({
        firstName,
        lastName,
        nickName,
        address,
        phone,
        men,
        women,
      }).then((data) => {
        //success callback
        console.log('data ', data)
      }).catch((error) => {
        //error callback
        console.log('error ', error)
      })
    }


    this.setState({
      firstName: '',
      lastName: '',
      nickName: '',
      address: '',
      phone: '',
      men: false,
      women: false,
      keyStaff: '',
    });
  }

  // deleteItem = (index, key) => {

  //   firebase.database().ref('/Account' + '/' + this.state.uid + '/Staff' + key).remove();

  // }

  edit = (index, key) => {
    let { staff } = this.state
    let staffEdit = staff.find(staff => staff.key === key)
    console.log("Pro", staffEdit)
    this.setState({
      firstName: staffEdit.firstName,
      lastName: staffEdit.lastName,
      nickName: staffEdit.nickName,
      address: staffEdit.address,
      phone: staffEdit.phone,
      men: staffEdit.men,
      women: staffEdit.women,
      keyStaff: key
    })
  }


  render() {
    const { firstQuery } = this.state;
    return (
      <View style={{ flex: 1 }}>

        <Content contentContainerStyle={{ flex: 1 }}>
          <Grid style={{ backgroundColor: "#FFFFFF" }}>
            <Col >
              <Row>
                <Col style={{ width: 100 }}></Col>
                {/* form */}
                <Col style={{ marginTop: 10 }}>
                  <ScrollView>
                    <TextInput
                      style={{ marginBottom: 20 }}
                      label='First name'
                      mode='outlined'
                      value={this.state.firstName}
                      onChangeText={text => this.setState({ firstName: text })}
                    />
                    <TextInput
                      style={{ marginBottom: 20 }}
                      label='Last name'
                      mode='outlined'
                      value={this.state.lastName}
                      onChangeText={text => this.setState({ lastName: text })}
                    />
                    <TextInput
                      style={{ marginBottom: 20 }}
                      label='Nickname'
                      mode='outlined'
                      value={this.state.nickName}
                      onChangeText={text => this.setState({ nickName: text })}
                    />
                    <TextInput
                      style={{ marginBottom: 20 }}
                      label='Address'
                      mode='outlined'
                      value={this.state.address}
                      onChangeText={text => this.setState({ address: text })}
                    />
                    <TextInput
                      style={{ marginBottom: 20 }}
                      label='Phone Number'
                      mode='outlined'
                      value={this.state.phone}
                      onChangeText={text => this.setState({ phone: text })}
                    />
                    <Card>
                      <CardItem header>
                        <Text>Please select gender</Text>
                      </CardItem>
                      <CardItem body>
                        <CheckBox checked={this.state.men}
                          onPress={() => this.menPressed()}
                          style={{ marginRight: 20 }}
                        />
                        <Text>Men</Text>
                      </CardItem>
                      <CardItem body>
                        <CheckBox checked={this.state.women}
                          onPress={() => this.womenPressed()}
                          style={{ marginRight: 20 }}
                        />
                        <Text>Women</Text>
                      </CardItem>
                    </Card>
                    <Row>
                      <Col>
                        <Button

                          mode="outlined"
                          onPress={() => this.clear()}
                          color="#e74c3c"
                          style={{ marginTop: 20, marginLeft: 20, marginRight: 20 }}
                        >
                          Clear
                    </Button>
                      </Col>
                      <Col>
                        <Button

                          mode="outlined"
                          onPress={() => this.save('')}
                          color="#2ecc71"
                          style={{ marginTop: 20, marginLeft: 20, marginRight: 20 }}
                        >
                          Save
                    </Button>
                      </Col>
                    </Row>
                  </ScrollView>
                </Col>
              </Row>
            </Col>

            {/* listname */}
            <Col style={{ marginTop: 10, marginRight: 100, marginLeft: 100 }}>
              {/* <Searchbar
                placeholder="Search"
                onChangeText={query => { this.setState({ firstQuery: query }); }}
                value={firstQuery}
              /> */}

              <ScrollView keyExtractor={(item, index) => index}>
                {

                  this.state.staff.map((item, index) => (

                    <View style={styles.rowOrderView} key={index}>

                      <Text style={styles.textItemOrder}>{item.firstName}  {item.lastName}</Text>
                      <View style={styles.itemInOrder}>
                        <B
                        onPress={() => this.edit(index, item.key)}
                          title="Edit"
                        />
                        <TouchableHighlight
                          onPress={() => this.deleteItem(index, item.key)}
                        >
                          <Icon style={styles.iconTrash} name="ios-trash" />
                        </TouchableHighlight>
                      </View>

                    </View>


                  ))
                }
              </ScrollView>
            </Col>

          </Grid>
        </Content>
      </View>
    );
  }
}


