
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  CheckBox,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  Button as B,
  DatePickerIOS,

} from 'react-native';
import {
  Form,
  Icon,
  Tab,
  Tabs,
  TabHeading,
  Header,
  Left,
  Right,
  Content,
  Container,
  Picker,
  Item,
} from 'native-base';


import styles from '../styles';
import { Col, Row, Grid } from "react-native-easy-grid";
import { TextInput, Button, Searchbar } from 'react-native-paper';
import DatePicker from 'react-native-datepicker'

import * as firebase from 'firebase';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from "rn-fetch-blob";
const Blob = RNFetchBlob.polyfill.Blob;
const fs = RNFetchBlob.fs;
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = Blob;



export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 'first',
      type: undefined,
      type2: undefined,
      name: '',
      price: '',
      firstQuery: '',
      data: ['name', 'name'],
      date: new Date(),
      point: 0,
      startDate: new Date(),
      endDate: new Date(),
      detail: '',
      numPoint: '',
      promotionData: [],
      photo: null,
      keyMenu: '',
      //{ uri: response.uri },
    }
  }

  componentDidMount() {
    firebase.database().ref('/').on('value', (data) => {
      let _p = data.toJSON().Promotion
      // console.log(p);

      let waitP = Object.keys(_p).length
      let x = 1
      let promotion = []
      Object.keys(_p).forEach((key, index) => {
        promotion.push({
          key: key,
          id: x++,
          name: _p[key].name,
          detail: _p[key].detail,
          numPoint: _p[key].numPoint,
          type: _p[key].type,
          img: _p[key].img,
          startDate: _p[key].startDate,
          endDate: _p[key].endDate,
        })
        if (waitP == index + 1) {
          // console.log("finish")
          // this.props.setBakeryDataAction(bakery)
          this.setState({
            promotionData: promotion
          })
          console.log("promotion", promotion);


        }
      })
    })
  }

  static navigationOptions = {
    drawerIcon: ({ tintColor }) => (
      <Icon name="settings" style={{ fontSize: 24, color: tintColor }} />
    )
  }
  onValueChange2(value) {
    this.setState({
      type: value
    });
  }
  onValueChange(value) {
    this.setState({
      type2: value
    });
  }
  clear() {
    this.setState({
      type: undefined,
      type2: undefined,
      name: '',
      detail: '',
      numPoint: '',
      type: '',
      startDate: new Date(),
      endDate: new Date(),
      img: '',
      photo: null,
    })
  }

  openImage = () => {
    const options = {
      title: 'Select Picture',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        console.log(source);


        this.setState({
          photo: source,
        });
      }
    });
  }
  uploadImage(uri, mime = 'image/jpg') {
    return new Promise((resolve, reject) => {
      const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri
      let uploadBlob = null
      let fileName = Date.now()
      const imageRef = firebase.storage().ref('Promotion').child(`${fileName}.jpg`)

      fs.readFile(uploadUri, 'base64')
        .then((data) => {
          return Blob.build(data, { type: `${mime};BASE64` })
        })
        .then((blob) => {
          uploadBlob = blob
          return imageRef.put(blob, { contentType: mime })
        })
        .then(() => {
          uploadBlob.close()
          return imageRef.getDownloadURL()
        })
        .then((url) => {
          resolve(url)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  onSave = (k) => {
    console.log("Save");
    let { photo, name, type, detail, startDate, endDate, numPoint, keyMenu } = this.state

    this.uploadImage(photo.uri).then(url => {
      console.log("URL Image: ", url);
      console.log("Nameee", name)
      if (keyMenu === '') {
        firebase.database().ref(`Promotion`).push({
          name: name,
          detail: detail,
          numPoint: parseInt(numPoint),
          type: type,
          startDate: startDate,
          endDate: endDate,
          img: url,

        }).then(() => {
          console.log('yess');

        }).catch(error => {
          console.log(error)
          alert('noooo')

        })
      } else {
        firebase.database().ref('Promotion/' + keyMenu).update({
          name: name,
          detail: detail,
          numPoint: parseInt(numPoint),
          type: type,
          startDate: startDate,
          endDate: endDate,
          img: url,

        }).then(() => {
          console.log('yess');

        }).catch(error => {
          console.log(error)
          alert('noooo')

        })
      }



      alert('uploaded');
    })
      .catch(error => {
        console.log(error)
        alert('uploaded')

      })
    this.setState({
      type: undefined,
      type2: undefined,
      name: '',
      detail: '',
      numPoint: '',
      type: '',
      startDate: new Date(),
      endDate: new Date(),
      img: '',
      photo: null,
      keyMenu: '',
    })
  }

  deleteItem = (index, key) => {
    // console.log("STAFF", this.state.staff)
    // console.log("INDEX",index)
    // console.log("KEY",key)

    firebase.database().ref('/Promotion' + '/' + key).remove();

  }

  edit = (index, key) => {
    let { promotionData } = this.state
    let proEdit = promotionData.find(promotionData => promotionData.key === key)
    console.log("Pro", proEdit)
    this.setState({
      name: proEdit.name,
      type: proEdit.type,
      numPoint: String(proEdit.numPoint),
      startDate: proEdit.startDate,
      endDate: proEdit.endDate,
      detail: proEdit.detail,
      photo: { uri: proEdit.img },
      keyMenu: key
    })
  }



  render() {
    const { firstQuery, photo } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <Content contentContainerStyle={{ flex: 1 }}>
          <View style={{ flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'row' }}>
            <View style={{ flex: 0.5 }}>
              <Col>
                <Col style={{ justifyContent: 'center' }}>
                  <Col style={{ marginLeft: 20, marginRight: 20, marginTop: 30 }}>
                    <ScrollView>



                      <TouchableOpacity onPress={this.openImage}>
                        <Image style={{ justifyContent: "center", width: 200, height: 200, marginLeft: 130 }} source={photo == null ? require('../img/image.png') : { uri: photo.uri }} />
                      </TouchableOpacity>


                      <TextInput
                        style={{ marginBottom: 20, marginTop: 20 }}
                        label='Name'
                        mode='outlined'
                        value={this.state.name}
                        onChangeText={name => this.setState({ name })}
                      />
                      <TextInput
                        style={{ marginBottom: 20 }}
                        label='Detail'
                        mode='outlined'
                        value={this.state.detail}
                        onChangeText={detail => this.setState({ detail })}
                      />

                      <TextInput
                        style={{ marginBottom: 20 }}
                        label='Point'
                        mode='outlined'
                        value={this.state.numPoint}
                        onChangeText={numPoint => this.setState({ numPoint })}
                      />



                      <Item picker >
                        <Picker
                          mode='dropdown'
                          iosIcon={<Icon name="arrow-down" />}
                          style={{ width: undefined, marginTop: 10, marginBottom: 10 }}
                          placeholder="Select type"
                          textStyle={{ color: "black" }}
                          itemStyle={{
                            backgroundColor: "white",
                            marginLeft: 0,
                            paddingLeft: 10
                          }}
                          itemTextStyle={{ color: 'black' }}
                          onChangeText={(type) => this.setState({ type })}
                          selectedValue={this.state.type}
                          onValueChange={this.onValueChange2.bind(this)}
                        >

                          <Picker.Item label="+ Point" value="addPoint" />
                          <Picker.Item label="- Point" value="minusPoint" />

                        </Picker>
                      </Item>

                      <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: 10 }}>

                        <Text style={{ fontSize: 18, marginTop: 18 }}>Start Date : </Text>
                        <DatePicker
                          style={{ width: 200, marginTop: 10, marginBottom: 10 }}
                          date={this.state.startDate}
                          mode="date"
                          placeholder="select date"
                          format="DD-MM-YYYY"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          customStyles={{
                            dateIcon: {
                              position: 'absolute',
                              left: 0,
                              top: 4,
                              marginLeft: 0
                            },
                            dateInput: {
                              marginLeft: 36
                            }
                            // ... You can check the source to find the other keys.
                          }}
                          onDateChange={(date) => {
                            this.setState({ startDate: date })


                          }}
                        />

                      </View>

                      <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: 10 }}>
                        <Text style={{ fontSize: 18, marginTop: 18 }}>End Date : </Text>
                        <DatePicker
                          style={{ width: 200, marginLeft: 5, marginTop: 10, marginBottom: 10 }}
                          date={this.state.endDate}
                          mode="date"
                          placeholder="select date"
                          format="DD-MM-YYYY"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          customStyles={{
                            dateIcon: {
                              position: 'absolute',
                              left: 0,
                              top: 4,
                              marginLeft: 0
                            },
                            dateInput: {
                              marginLeft: 36
                            }
                            // ... You can check the source to find the other keys.
                          }}
                          onDateChange={(date) => {
                            this.setState({ endDate: date })


                          }}
                        />
                      </View>

                      <Row>
                        <Col>
                          <Button

                            mode="outlined"
                            onPress={() => this.clear()}
                            color="#e74c3c"
                            style={{ marginTop: 20, marginLeft: 20, marginRight: 20, marginBottom: 20 }}
                          >
                            Clear
                    </Button>
                        </Col>
                        <Col>
                          <Button

                            mode="outlined"
                            // onPress={this.onSave(true)}
                            onPress={() => this.onSave('')}
                            color="#2ecc71"
                            style={{ marginTop: 20, marginLeft: 20, marginRight: 20, marginBottom: 20 }}
                          >
                            Save
                    </Button>
                        </Col>
                      </Row>

                    </ScrollView>

                  </Col>
                </Col>

              </Col>
            </View>

            <View style={{ flex: 0.5 }}>


              {/* Listmenu */}
              <Col style={{ backgroundColor: '#BEBEBE' }}>
                {/* <Searchbar
                  style={{ marginLeft: 30, marginRight: 30, marginTop: 30 }}
                  placeholder="Search"
                  onChangeText={query => { this.setState({ firstQuery: query }); }}
                  value={firstQuery}
                /> */}
                <ScrollView keyExtractor={(item, index) => index} style={{ marginLeft: 30, marginRight: 30 }}>
                  {

                    this.state.promotionData.map((item, index) => (

                      <View style={styles.rowOrderView} key={index}>

                        <Text style={styles.textItemOrder}>{item.name}</Text>
                        <View style={styles.itemInOrder}>
                          <B
                            onPress={() => this.edit(index, item.key)}
                            title="Edit"
                          />
                          <TouchableHighlight onPress={() => this.deleteItem(index, item.key)}>
                            <Icon style={styles.iconTrash} name="ios-trash" />
                          </TouchableHighlight>
                        </View>

                      </View>


                    ))
                  }
                </ScrollView>
              </Col>
            </View>

          </View>
        </Content>

      </View>
    );
  }
}

