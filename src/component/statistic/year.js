import React, { Component } from 'react';
import { ECharts } from 'react-native-echarts-wrapper';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    CheckBox,
    Image,
    TouchableOpacity,
    TouchableHighlight
} from 'react-native';
import {
    Form,
    Icon,
    Tab,
    Tabs,
    TabHeading,
    Header,
    Left,
    Right,
    Content,
    Container,
    Picker,
    Item,

} from 'native-base';
import styles from '../styles';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: ['Capuchino', 'Moccha'],
            sumCup: 100,

        }
    }

    option = {
        xAxis: {
            type: 'category',
            data: ['Jan', 'Feb', 'Mar', 'Api', 'May', 'Jun', 'July', 'Aug', 'Seb', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            type: 'value'
        },
        series: [{
            data: [820, 932, 901, 934, 1290, 1330, 1320, 550, 909, 789, 1234, 300, 345],
            type: 'line'
        }]
    };

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'row' }}>

                <View style={{ flex: 0.7, justifycontent: 'center' }}>
                    <ECharts option={this.option}></ECharts>
                </View>
                <View style={{ flex: 0.3, justifycontent: 'center', flexDirection: 'column', marginLeft: 5, marginTop: 10 }}>


                    <View View style={{ flexDirection: 'row', flex: 0.1, justifycontent: 'center' }}>
                        <Icon name="ios-heart" style={{ fontSize: 30, textAlign: 'center', marginLeft: 80, marginTop: 10 }} />
                        <Text style={{ fontSize: 20, textAlign: 'center', marginLeft: 10,marginTop: 10 }}>Best Seller</Text>
                    </View>

                    <View style={{ flexDirection: 'column', flex: 0.9 }}>
                        <ScrollView keyExtractor={(item, index) => index} style={{ marginLeft: 5, marginRight: 30 }}>
                            {

                                this.state.data.map((item, index) => (

                                    <View style={styles.rowBestView} key={index}>

                                        <Text style={styles.textItemOrder}>{item}</Text>
                                        {/* <Text style={styles.iconBest}> {this.state.sumCup}  Cup </Text> */}


                                    </View>


                                ))
                            }
                        </ScrollView>
                    </View>

                </View>

            </View>
        );
    }
}

