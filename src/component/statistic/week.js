import React, { Component } from 'react';
import { ECharts } from 'react-native-echarts-wrapper';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    CheckBox,
    Image,
    TouchableOpacity,
    TouchableHighlight,
    Alert
} from 'react-native';
import {
    Form,
    Icon,
    Tab,
    Tabs,
    TabHeading,
    Header,
    Left,
    Right,
    Content,
    Container,
    Picker,
    Item,

} from 'native-base';
import styles from '../styles';
import * as firebase from 'firebase';
import Firebase from '../../Firebase/firebase'
import DatePicker from 'react-native-datepicker'

if (!firebase.apps.length) {
    firebase.initializeApp(Firebase.FirebaseConfig);
}

let optionDefult =
{
    xAxis: {
        type: 'category',
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

    },
    yAxis: {
        type: 'value'
    },
    series: [{
        data: [0, 0, 0, 0, 0, 0, 0],
        type: 'bar'
    }]
}
export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: ['Capuchino', 'Moccha'],
            sumCup: 100,

            option: {
                xAxis: {
                    type: 'category',
                    data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    data: [0, 0, 0, 0, 0, 0, 0],
                    type: 'bar'
                }]
            },

            total: 0,

            date: new Date()
        }

        this.setDate = this.setDate.bind(this);
    }

    componentDidMount() {
        let d = new Date().getDate()
        let m = new Date().getMonth() + 1
        let y = new Date().getFullYear()



        firebase.database().ref('/Order/' + y + '/' + m + '/' + d).once('value', (data) => {

            let total = data.toJSON().totalPrice
            console.log(total)
            this.setState({
                total
            })

        })
    }

    updateData(date) {
        let splitDate = date.split('-')

        // let d = d.getDate()
        let d = parseInt(splitDate[0])
        let m = parseInt(splitDate[1])
        let y = parseInt(splitDate[2])

        // // firebase.database().ref('/').on('value', (data) => {
        firebase.database().ref('/Order/' + y + '/' + m + '/' + d).once('value', (data) => {

            // console.log(data.val())
            // if (data.val() === null) {
            //     Alert.alert("Alert", "Not data");
            // } else {
            let total = data.toJSON().totalPrice
            console.log(total)
            this.setState({
                total
            })
            // }


        })
    }

    setDate(newDate) {
        this.setState({ chosenDate: newDate });
    }


    render() {
        return (
            <View style={{ flex: 1, justifycontent: 'center', flexDirection: 'column' }}>

                <View style={{ flex: 1, justifycontent: 'center', flexDirection: 'column', marginTop: 20 }}>
                    <DatePicker
                        style={{ width: 200, marginLeft: 400 }}
                        date={this.state.date}
                        mode="date"
                        placeholder="select date"
                        format="DD-MM-YYYY"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                            dateIcon: {
                                position: 'absolute',
                                left: 0,
                                top: 4,
                                marginLeft: 0
                            },
                            dateInput: {
                                marginLeft: 36
                            }
                            // ... You can check the source to find the other keys.
                        }}
                        onDateChange={(date) => {
                            this.updateData(date)
                            this.setState({ date: date })


                        }}
                    />

                    <View style={{ flex: 0.2, flexDirection: 'row', justifyContent: 'center', marginTop: 40 }}>
                        <View style={{
                            backgroundColor: '#29ab87',flexDirection: 'row',justifyContent: 'center',
                            shadowOffset: {
                                width: 5,
                                height: 5,
                            },
                            shadowColor: 'black',
                            shadowOpacity: 1.0,
                            width: 600,
                            height: 200,
                            marginLeft: 20,
                            marginTop: 20
                        }}>

                            <Text style={{ fontSize: 25, marginLeft: 5,marginTop: 40 ,color: 'white'}}>ยอดขายวันนี้ : </Text>
                            <Text style={{ fontSize: 100, marginLeft: 5,marginTop: 40,color: 'white' }}>{this.state.total} บาท.</Text>
                        </View>

                    </View>

                </View>

            </View>
        );
    }
}

