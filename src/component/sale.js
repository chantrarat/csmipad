//หน้านี้จะเป็น component หลักให้ที่อื่นเรียกใช้ state กับ function

import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  Button,
  FlatList,
  TextInput,
  Alert,
  Modal,
  ScrollView,
  Image,
  Switch,
} from 'react-native';

import {
  Form,
  Icon,
  Tab,
  Tabs,
  TabHeading,
  Header,
  Left,
  Right,

} from 'native-base';
import { CheckBox } from 'react-native-elements'
import Logout from '../../App'
import { Dropdown } from 'react-native-material-dropdown';



// -------------------------------------ที่ import ใหม่----------------------------------------- //
import styles from './styles'
import Tab1 from './Tab/tabCoffee'
import Tab2 from './Tab/tabTea'
import Tab3 from './Tab/tabSoda'
import Tab4 from './Tab/tabCocoa'
import Tab5 from './Tab/tabMilk'
import Tab6 from './Tab/tabBakery'
import customer from './customer';
// import compose from 'recompose/compose'

import { connect } from 'react-redux'
import {
  addItem,
  updateList,
  setCoffeeData,
  setTeaData,
  setNonCoffeeData,
  addBill,
  setBakeryData,
  clearItem
} from '../Action'

import * as firebase from 'firebase';
import Firebase from '../Firebase/firebase'

//  import firebase from '../Firebase/firebase';

//Firebase----------------------------------------------------------------
// const config = {
//   apiKey: "AIzaSyA97_t_SjEsNptuGIkljPymPDL-ch4BZ90",
//   authDomain: "fir-life-roater.firebaseapp.com",
//   databaseURL: "https://fir-life-roater.firebaseio.com",
//   projectId: "fir-life-roater",
//   storageBucket: "fir-life-roater.appspot.com",
//   messagingSenderId: "424729769899"
// }

if (!firebase.apps.length) {
  firebase.initializeApp(Firebase.FirebaseConfig);
}
//Firebase----------------------------------------------------------------

class App extends Component {


  constructor(props) {
    super(props);
    this.state = {
      buttonCheckOut: "Check Out",
      orderList: [],


      billNumber: "Bill No :",
      orderEx: [],
      // numberPieces: 0,

      coffeeData: [],
      noncoffeeData: [],
      teaData: [],
      sodaData: [],
      cocoaData: [],
      milkData: [],
      bakeryData: [],
      promotionData: [],
      staffData: [],

      coffee: [],

      modalOption: false,
      modalCheckout: false,

      type: "",
      level: "",
      shot: 1,
      sweet: "",
      milk: "",
      topping: "",
      size: "",
      numPiece: 1,

      modalPromotion: false,

      //checkout
      phoneNumber: "",
      billNo: "",
      date: "",
      time: "",
      by: "",
      // { value: 'Jun' }, { value: 'Bell' }
      staff: [],
      customer: "",
      total: 0.00,
      discount: 5.00,
      cash: 0.00,
      change: 0.00,
      checked: false,
      printBill: false,
      uid: "",
      point: 0.00,
      prePoint: 0.00,

      statusLogin: true,
      uid: '-LbcLhMM8so9uBKKCzbl',
      calPoint: [],

    }
    this.tempID = -1
    // firebase.initializeApp(config);
  }

  componentDidMount() {

    //pull data from data base
    firebase.database().ref('/').on('value', (data) => {

      let c = data.toJSON().coffee
      let waitC = Object.keys(c).length
      console.log("wait", waitC);
      let i = 1
      let coffee = []
      Object.keys(c).forEach((key, index) => {
        coffee.push({ id: i++, name: c[key].name, price: c[key].price, type: c[key].type, src: c[key].imagePath })
        if (waitC == index + 1) {
          console.log("finish")
          this.props.setCoffeeDataAction(coffee)
        }
      })

      let nc = data.toJSON().nonCoffee
      let waitNc = Object.keys(nc).length
      console.log("wait", waitNc);
      let j = 1
      let nonCoffee = []
      Object.keys(nc).forEach((key, index) => {
        nonCoffee.push({ id: j++, name: nc[key].name, price: nc[key].price, type: nc[key].type, src: nc[key].imagePath })
        if (waitNc == index + 1) {
          console.log("finish")
          this.props.setNonCoffeeDataAction(nonCoffee)
        }
      })

      let b = data.toJSON().bakery
      let waitB = Object.keys(b).length
      console.log("wait", waitB);
      let k = 1
      let bakery = []
      Object.keys(b).forEach((key, index) => {
        bakery.push({ id: k++, name: b[key].name, price: b[key].price, type: b[key].type, src: b[key].imagePath })
        if (waitB == index + 1) {
          console.log("finish")
          this.props.setBakeryDataAction(bakery)

        }
      })

      let p = data.toJSON().Promotion
      let waitP = Object.keys(p).length
      let x = 1
      let promotion = []
      Object.keys(p).forEach((key, index) => {
        promotion.push({
          key: key,
          id: x++,
          name: p[key].name,
          detail: p[key].detail,
          numPoint: p[key].numPoint,
          type: p[key].type,
          img: p[key].img,
          startDate: p[key].startDate,
          endDate: p[key].endDate,
        })
        if (waitP == index + 1) {
          // console.log("finish")
          // this.props.setBakeryDataAction(bakery)
          this.setState({
            promotionData: promotion
          })

        }
      })

    })

    firebase.database().ref('/Account' + '/' + this.state.uid + '/Staff').on('value', (data) => {
      let s = data.toJSON()
      let waitS = Object.keys(s).length
      let y = 1
      let staff = []
      Object.keys(s).forEach((key, index) => {
        staff.push({ value: s[key].firstName })
        if (waitS == index + 1) {
          console.log(staff);

          this.setState({
            // staff: [{ value: 'Jun' }, { value: 'Bell' }],
            staffData: staff
          })

        }
      })
    })
  }

  componentWillReceiveProps(nextProps) {
    // console.log(nextProps);  
    if (nextProps.orderList.length != 0) {
      this.setState({
        orderList: [...nextProps.orderList]
      })

    }
    if (nextProps.coffeeData.length != 0) {
      this.setState({
        coffeeData: [...nextProps.coffeeData]
      })
    }
    if (nextProps.noncoffeeData.length != 0) {
      this.setState({
        noncoffeeData: [...nextProps.noncoffeeData]

      })
    }
    if (nextProps.bakeryData.length != 0) {
      this.setState({
        bakeryData: [...nextProps.bakeryData]

      })
    }
    if (nextProps.teaData.length != 0) {
      this.setState({
        teaData: [...nextProps.teaData]

      })
    }
    if (nextProps.sodaData.length != 0) {
      this.setState({
        sodaData: [...nextProps.sodaData]

      })
    }
    if (nextProps.cocoaData.length != 0) {
      this.setState({
        cocoaData: [...nextProps.cocoaData]

      })
    }
    if (nextProps.milkData.length != 0) {
      this.setState({
        milkData: [...nextProps.milkData]

      })
    }
    // if (nextProps.noncoffeeData.length != 0) {
    //   this.setState({
    //     noncoffeeData: [...nextProps.noncoffeeData]

    //   })
    // }
    // if (nextProps.noncoffeeData.length != 0) {
    //   this.setState({
    //     noncoffeeData: [...nextProps.noncoffeeData]

    //   })
    // }
  }
  wantPrint = () => {
    this.setState({
      printBill: true
    })
  }
  print = () => {
    Alert.alert(
      'You want print recieve?',
      '',
      [
        // { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'OK', onPress: () => this.wantPrint() },
      ],
      { cancelable: false }
    )
  }
  pay = (visible) => {

    let {
      //Data
      orderList,
      //State 
      billNo,
      date,
      time,
      by,
      customer,
      prePoint,
      point,
      total,
      discount,
      cash,
      change,
      printBill,
    } = this.state


    this.props.addBillAction({
      billNo,
      date,
      time,
      by,
      customer,
      total,
      discount,
      cash,
      change,
      orderList,
      printBill
    })

    // writeUserData(email,fname,lname)
    // firebase.database().ref('/').on('value', (data) => {
    let splitDate = date.split('/')
    firebase.database().ref('Order/' + splitDate[2] + '/' + splitDate[1] + '/' + splitDate[0]).push({
      billNo,
      date,
      time,
      by,
      customer,
      total,
      discount,
      cash,
      change,
      orderList,
      printBill
    }).then((data) => {
      //success callback
      console.log('data ', data)
      firebase.database().ref('Order/' + splitDate[2] + '/' + splitDate[1] + '/' + splitDate[0]).once('value', (data) => {
        let t = data.toJSON().totalPrice
        if (t) {
          console.log('if', t)

          firebase.database().ref('Order/' + splitDate[2] + '/' + splitDate[1] + '/' + splitDate[0]).update(
            {
              totalPrice: parseInt(t) + total,
            })
        } else {
          console.log('else', t)
          firebase.database().ref('Order/' + splitDate[2] + '/' + splitDate[1] + '/' + splitDate[0]).update({
            totalPrice: total
          })
        }
      })

      firebase.database().ref('Customer/' + this.state.uid).update(
        {
          point: prePoint + point,
        })

      firebase.database().ref('Customer/' + this.state.uid + '/' + 'orderHistory/' + splitDate[2] + '/' + splitDate[1] + '/' + splitDate[0]).push({
        // firebase.database().ref('Customer/'+this.state.uid + '/'+'orderHistory/'+ '2020' + '/' + '3' + '/' + '6').push({
        billNo,
        date,
        time,
        by,
        customer,
        total,
        discount,
        cash,
        change,
        orderList,
        printBill

      })

    }).catch((error) => {
      //error callback
      console.log('error ', error)
    })

    this.props.clearItemAction()
    this.setState({
      modalCheckout: visible,
      orderList: [],
      cash: 0,
      printBill: false,
      phoneNumber: "",
      customer: "",
      point: 0,
      change: 0,
      prePoint: 0
    });
  }

  getType = (item) => {
    this.setState({ type: item });
    Alert.alert(item);
  }
  getLevel = (item) => {
    this.setState({ level: item });
    Alert.alert(item);
  }

  reduceShot = (shot) => {
    if (shot == 1) {
      Alert.alert("Do not reduce shot!");
    } else {
      this.setState({ shot: this.state.shot - 1 });
    }

  }

  addShot = (shot) => {
    if (shot == 4) {
      Alert.alert("Do not add shot!");
    } else {
      this.setState({ shot: this.state.shot + 1 });
    }
  }

  getSweet = (item) => {
    this.setState({ sweet: item });
    Alert.alert(item);
  }
  getMilk = (item) => {
    this.setState({ milk: item });
    Alert.alert(item);
  }
  getTopping = (item) => {
    this.setState({ topping: item });
    Alert.alert(item);
  }
  getSize = (item) => {
    this.setState({ size: item });
    Alert.alert(item);
  }
  showOptionModal = (visible, id, itemType) => {
    console.log(visible);
    console.log(id);
    console.log(itemType);


    if (itemType === 'cancle') {
      this.setState({
        modalOption: visible,
      });

    } else {

      if (itemType !== 'bakery') {

        if (!visible) {
          let {
            //Data
            orderList,
            coffeeData,
            teaData,
            sodaData,
            cocoaData,
            milkData,
            bakeryData,
            //State 
            shot,
            type,
            level,
            sweet,
            milk,
            topping,
            size,
            numPiece
          } = this.state

          let orderItem = {}

          switch (itemType) {
            case "coffee":
              orderItem = coffeeData.find(obj => obj.id === this.tempID)
              // console.log(orderItem)
              break;
            case "tea":
              orderItem = teaData.find(obj => obj.id === this.tempID)

              break;
            case "soda":
              orderItem = sodaData.find(obj => obj.id === this.tempID)

              break;
            case "cocoa":
              orderItem = cocoaData.find(obj => obj.id === this.tempID)

              break;
            case "milk":
              orderItem = milkData.find(obj => obj.id === this.tempID)

              break;
            case "bakery":
              orderItem = bakeryData.find(obj => obj.id === this.tempID)

              break;

            default:
              break;
          }


          this.props.addItemAction({
            orderItem,
            type,
            level,
            numShot: shot,
            sweet,
            milk,
            topping,
            size,
            numPiece: 1,
          })
        } else {
          this.tempID = id

        }
        this.setState({
          modalOption: visible,
          cash: 0,
          shot: 1
        });
      }
      else {

        let {
          //Data
          orderList,
          coffeeData,
          teaData,
          sodaData,
          cocoaData,
          milkData,
          bakeryData,
          //State 
          shot,
          type,
          level,
          sweet,
          milk,
          topping,
          size,
          numPiece
        } = this.state

        let orderItem = {}

        orderItem = bakeryData.find(obj => obj.id === id)
        if (orderItem) {
          this.props.addItemAction({
            orderItem,
            type,
            level,
            numShot: shot,
            sweet,
            milk,
            topping,
            size,
            numPiece: 1,
          })
        }

      }
    }
  }

  showPromotionModal = (visible) => {
    this.setState({
      modalPromotion: visible,
    })
  }
  checkCalPoint = (index, k) => {
    Alert.alert(
      'You want use this promotion?',
      '',
      [
        // { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'OK', onPress: () => this.calPoint(k) },
      ],
      { cancelable: false }
    )
  }
  calPoint = (k) => {
    firebase.database().ref('Promotion/' + k).once('value', (data) => {
      let select = data.toJSON()

      if (select.type === "addPoint") {
        let sum = this.state.point + select.numPoint
        this.setState({
          point: sum
        })
      } else {
        if (this.state.prePoint >= select.numPoint) {
          let sum = this.state.prePoint - select.numPoint
          this.setState({
            prePoint: sum
          })
        } else {
          alert("You can't use point")
        }
      }
    })
  }

  showCheckoutModal = (visible) => {
    if (!visible) {
      this.setState({
        modalCheckout: visible,
        cash: 0,
        printBill: false,
        phoneNumber: "",
        customer: "",
        point: 0,
        change: 0,
        prePoint: 0
      })
    } else {

      let _tempTotal = 0;
      [...this.state.orderList].map(obj => _tempTotal += obj.orderItem.price)
      console.log(_tempTotal)
      let billPoint = _tempTotal * 0.1
      let tempDate = new Date()




      //current date
      let d = tempDate.getDate()
      let mo = tempDate.getMonth() + 1
      let y = tempDate.getFullYear()
      //current time
      let h = tempDate.getHours()
      let min = tempDate.getMinutes()
      let s = tempDate.getSeconds()

      //find customer
      firebase.database().ref('/Customer')
        .orderByChild('phoneNumber')
        .equalTo(this.state.phoneNumber)
        .once('value', (data) => {

          if (data.val() != null) {
            console.log("DATA", data.val());
            let cus = data.val()
            let uid = Object.keys(cus)[0]
            let name = Object.values(cus)[0].firstName
            let point = Object.values(cus)[0].point

            this.setState({
              customer: name,
              prePoint: point,
              uid
            })
            console.log("Data Customer", cus, "Uid is", uid)
          } else {
            console.log("null")
          }
        })

      this.setState({
        modalCheckout: visible,
        total: _tempTotal,
        date: d + '/' + mo + '/' + y,
        time: h + ':' + min + ':' + s,
        billNo: d + mo + y + h + min + s + d + mo + y + h + min + s,
        point: billPoint
      });

    }


  }

  reducePiece = (index) => {
    let { orderList } = this.state
    orderList[index].numPiece--
    this.setState({
      orderList,
    });
  }
  addPiece = (index) => {
    let { orderList } = this.state
    orderList[index].numPiece++
    this.setState({
      orderList,
    });
    // console.log(orderList);

  }

  deleteItem = (index) => {
    let { orderList } = this.state
    orderList.splice(index, 1)
    this.setState({
      orderList
    });
    this.props.updateListAction(orderList)
  }

  static navigationOptions = {
    drawerIcon: ({ tintColor }) => (
      <Icon name="ios-home" style={{ fontSize: 24, color: tintColor }} />
    )
  }

  checked = () => {
    Alert.alert(
      'You want logout?',
      '',
      [
        // { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'OK', onPress: () => this.logout('ok') },
      ],
      { cancelable: false }
    )
  }

  logout = (value) => {
    firebase.database().ref('/Account')
      .once('value', (data) => {
        if (data.val() != null) {
          let d = data.val()
          let key = Object.keys(d)[0]
          if (value === 'ok') {

            firebase.database().ref().child('/Account/' + key).update(
              {
                statusLogin: false,
              })
            this.setState({
              statusLogin: false,
            })
          } else {
            this.setState({
              statusLogin: true,
            })
          }
        } else {
          console.log("null")
        }

      })
  }

  showComponent = () => {
    if (this.state.statusLogin) {
      return <View style={styles.mainBox}>
        <Header style={{ backgroundColor: '#29ab87' }}>
          <Left>
            <Icon name="ios-menu" onPress={() => this.props.navigation.openDrawer()} />
          </Left>
          <Right>
            <Icon name="lock" onPress={() => this.checked()} />
          </Right>
          {/* <Text style={{ fontSize: 18 }}>Sale</Text> */}
        </Header>
        {/* ============================== SALE BOX ============================== */}
        <View style={styles.saleBox}>
          {/* ============================== ORDER LIST BOX ============================== */}
          <View style={styles.orderListBox}>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalCheckout}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
              }}>
              {/* ============================== BILL ============================== */}
              <View style={{ flex: 1, marginTop: 22, flexDirection: 'row', }}>
                <View style={{ flex: 0.5, marginTop: 22, flexDirection: 'column', }}>
                  <View style={{
                    backgroundColor: '#f1f1ff',
                    shadowOffset: {
                      width: 5,
                      height: 5,
                    },
                    shadowColor: 'black',
                    shadowOpacity: 1.0,
                    width: 400,
                    height: 670,
                    marginLeft: 50,
                    marginTop: 20
                  }}>
                    <View style={{ marginTop: 20, marginLeft: 10 }}>
                      <Text style={{ fontSize: 20, marginBottom: 5 }}> bill No: {this.state.billNo} </Text>
                      <Text style={{ fontSize: 20, marginBottom: 5 }}> Date: {this.state.date} </Text>
                      <Text style={{ fontSize: 20, marginBottom: 5 }}> Time: {this.state.time} </Text>
                      <Text style={{ fontSize: 20 }}> Customer: {this.state.customer}       Your point: {this.state.prePoint}</Text>
                      <View style={{ flexDirection: 'row' }}>
                        <Text style={{ fontSize: 20, marginBottom: 5, marginTop: 35 }}> By :   </Text>
                        <View style={{ width: 200 }}>
                          <Dropdown
                            label='Staff'
                            data={this.state.staffData}
                            fontSize={20}
                            textColor='black'
                            onChangeText={text => this.setState({ by: text })}
                          />
                        </View>
                      </View>




                      {/* {this.state.by} */}
                    </View>



                    <View style={styles.rowOrderView}>
                      <Text style={{ fontSize: 20 }}>Qty</Text>
                      <Text style={{ fontSize: 20, marginLeft: 20 }}>Description</Text>
                      <View style={styles.itemInOrder}>
                        <Text style={{ fontSize: 20, marginLeft: 15 }}>Amount(B.)</Text>
                      </View>
                    </View>

                    <ScrollView keyExtractor={(item, index) => index}>
                      {
                        this.state.orderList.map((item, index) => (
                          <View style={styles.rowOrderView} key={index}>

                            <Text style={{ fontSize: 20, marginLeft: 12 }}>{item.numPiece}</Text>
                            <Text style={{ fontSize: 20, marginLeft: 30 }}>{item.orderItem.name}</Text>

                            <View style={styles.itemInOrder}>
                              <Text style={{ fontSize: 20 }}>{item.orderItem.price}</Text>
                            </View>
                          </View>


                        ))
                      }
                    </ScrollView>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}> Total : </Text>
                      <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}>{this.state.total}</Text>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}> Your Point : </Text>
                      <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}>{this.state.prePoint}</Text>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}> Cash : </Text>
                      <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}>{this.state.cash}</Text>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}> Point : </Text>
                      <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}>{this.state.point}</Text>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}> Total point : </Text>
                      <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}>{this.state.point + this.state.prePoint}</Text>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}> Change : </Text>
                      <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}>{this.state.change}</Text>

                    </View>

                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ fontSize: 50 }}> Change     </Text>
                      <Text style={{ fontSize: 50, marginBottom: 10, marginLeft: 10 }}>{this.state.change}.</Text>
                    </View>

                  </View>
                </View>
                <View style={{ flex: 0.5, marginTop: 43, flexDirection: 'column', marginRight: 20 }}>
                  <View style={{ flex: 0.2, flexDirection: 'row', justifyContent: 'flex-end', backgroundColor: '#f1f1ff' }}>
                    <Text style={{ fontSize: 100, marginRight: 5 }}>{this.state.total}.</Text>
                  </View>
                  <View style={{ flex: 0.25, marginTop: 40, flexDirection: 'row', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 60, marginRight: 15, }}>Cash: </Text>
                    <TextInput
                      keyboardType={'number-pad'}
                      style={{ height: 80, marginLeft: 5, padding: 8, fontSize: 80 }}
                      placeholder={`${this.state.cash}`}
                      onChangeText={(text) => {
                        let t = this.state.total
                        let sum = text - t

                        this.setState({
                          cash: text,
                          change: sum,
                        })
                      }
                      }
                    // value={`${this.state.cash}`}

                    />
                  </View>

                  <View style={{ flex: 0.5, flexDirection: 'column', justifyContent: 'center', marginBottom: 30 }}>

                    <View style={{ flex: 0.25, flexDirection: 'row', justifyContent: 'center' }}>


                      {/* <View style={{ flex: 0.3, backgroundColor: '#f1f1ff', justifyContent: 'center', marginLeft: 5, }}>
                        <TouchableHighlight>
                          <Text style={{ fontSize: 20, textAlign: 'center', color: 'black' }}>50</Text>
                        </TouchableHighlight>
                      </View>



                      <View style={{ flex: 0.3, backgroundColor: '#f1f1ff', justifyContent: 'center', marginLeft: 5, }}>
                        <TouchableHighlight>
                          <Text style={{ fontSize: 20, textAlign: 'center', color: 'black' }}>100</Text>
                        </TouchableHighlight>
                      </View>



                      <View style={{ flex: 0.3, backgroundColor: '#f1f1ff', justifyContent: 'center', marginLeft: 5, }}>
                        <TouchableHighlight>
                          <Text style={{ fontSize: 20, textAlign: 'center', color: 'black' }}>500</Text>
                        </TouchableHighlight>
                      </View>



                      <View style={{ flex: 0.3, backgroundColor: '#f1f1ff', justifyContent: 'center', marginLeft: 5, }}>
                        <TouchableHighlight>
                          <Text style={{ fontSize: 20, textAlign: 'center', color: 'black' }}>1000</Text>
                        </TouchableHighlight>
                      </View>
 */}

                    </View>
                    <View style={{ flex: 0.25, flexDirection: 'row', justifyContent: 'center', marginTop: 40, }}>


                      {/* <Text style={{ fontSize: 25 }}>USE POINT</Text> */}

                      {/* <View style={{ marginLeft: 40 }}> */}
                      <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.state.modalPromotion}
                        onRequestClose={() => {
                          Alert.alert('Modal has been closed.');
                        }}>
                        <View style={{ flex: 1, justifyContent: 'center', marginTop: 30 }}>
                          <View style={{ flex: 9, justifyContent: 'center' }}>
                            <FlatList
                              data={this.state.promotionData}
                              renderItem={({ item, index }) => (
                                <View style={{ flex: 1, flexDirection: 'column', margin: 25 }}>
                                  <TouchableHighlight onPress={() => { this.checkCalPoint(index, item.key); }}>
                                    <Image style={styles.imagePromotion} source={{ uri: item.img }} />
                                  </TouchableHighlight>
                                  <Text style={{ textAlign: 'center', fontSize: 20 }}> {item.name}</Text>

                                </View>
                              )}
                              //Setting the number of column
                              numColumns={4}
                              keyExtractor={(item, index) => index}
                            />
                          </View>

                          <View style={{ flex: 1, backgroundColor: '#ffd500', justifyContent: 'center' }}>
                            <TouchableHighlight
                              onPress={() => {
                                this.showPromotionModal(!this.state.modalCheckout);
                              }}>
                              <Text style={{ fontSize: 30, textAlign: 'center', color: 'white' }}>closed</Text>

                            </TouchableHighlight>
                          </View>
                        </View>
                      </Modal>

                      <TouchableOpacity
                        onPress={() => {
                          this.showPromotionModal(true);
                        }}
                      >
                        <Text style={{ fontSize: 25 }}>PROMOTION</Text>
                      </TouchableOpacity>
                      {/* </View> */}

                    </View>




                    <View style={{ flex: 0.5, backgroundColor: '#f1f1ff', justifyContent: 'center' }}>
                      <TouchableOpacity
                        onPress={() => {
                          this.print();
                        }}
                      >

                        <Icon style={{ fontSize: 40, textAlign: 'center' }} name="ios-print" />
                      </TouchableOpacity>

                      <TouchableOpacity
                        onPress={() => { this.print(); }}>
                        <Text style={{ fontSize: 20, textAlign: 'center', color: 'black' }}>Print bill</Text>
                      </TouchableOpacity>
                    </View>

                  </View>


                  <View style={{ flex: 0.15, justifyContent: 'center', flexDirection: 'row', marginBottom: 30 }}>
                    <View style={{ flex: 0.5, backgroundColor: '#ed1c24', justifyContent: 'center', }}>
                      <TouchableHighlight
                        onPress={() => {
                          this.showCheckoutModal(!this.state.modalCheckout);
                        }}>
                        <Text style={{ fontSize: 30, textAlign: 'center', color: 'white' }}>CANCLE</Text>

                      </TouchableHighlight>
                    </View>
                    <View style={{ flex: 0.5, backgroundColor: '#ffd500', justifyContent: 'center', marginLeft: 5 }}>
                      <TouchableHighlight
                        onPress={() => {
                          this.pay(!this.state.modalCheckout);
                        }}>
                        <Text style={{ fontSize: 30, textAlign: 'center', color: 'white' }}>PAY</Text>

                      </TouchableHighlight>
                    </View>
                  </View>
                </View>
              </View>
              {/* ============================== END BILL ============================== */}
            </Modal>



            <View style={{ flex: 0.2, flexDirection: 'column' }}>
              <Form style={{ marginTop: 30, flex: 1, }}>
                <View style={{ marginTop: 5, flex: 0.6, flexDirection: 'row', marginLeft: 10, justifyContent: 'center' }}>
                  <Icon style={{ fontSize: 40 }} name="ios-contact" />
                  <TextInput
                    style={{ height: 40, marginLeft: 5, padding: 8, fontSize: 25 }}
                    value={this.state.phoneNumber}
                    placeholder="phone number"
                    keyboardType={'number-pad'}
                    onChangeText={(text) => { this.setState({ phoneNumber: text }) }}
                  />
                </View>

              </Form>
            </View>

            <View style={{ flex: 0.7, backgroundColor: '#FFFFFF' }}>
              <Button title="Order List" />
              <ScrollView keyExtractor={(item, index) => index}>
                {

                  this.state.orderList.map((item, index) => (

                    <View style={styles.rowOrderView} key={index}>

                      <Text style={styles.textItemOrder}>{item.orderItem.name}</Text>
                      <View style={styles.itemInOrder}>
                        <TouchableHighlight onPress={() => this.reducePiece(index)}>
                          <Icon style={styles.iconRemove} name="ios-remove" />
                        </TouchableHighlight>
                        <Text style={styles.numberPieces}>{item.numPiece}</Text>
                        <TouchableHighlight onPress={() => this.addPiece(index)}>
                          <Icon style={styles.iconAdd} name="ios-add" />
                        </TouchableHighlight>
                        <TouchableHighlight onPress={() => this.deleteItem(index)}>
                          <Icon style={styles.iconTrash} name="ios-trash" />
                        </TouchableHighlight>
                      </View>

                    </View>


                  ))
                }
              </ScrollView>
            </View>

            <View style={{ flex: 0.1, flexDirection: 'column', backgroundColor: '#29ab87', justifyContent: 'center', marginTop: 15 }}>
              <Form style={{ backgroundColor: '#29ab87', }}>
                {/* <Button title={this.state.buttonCheckOut} /> */}
                <TouchableHighlight onPress={() => { this.showCheckoutModal(true); }}>
                  <Text style={{ color: '#ffffff', fontSize: 25, textAlign: 'center' }}>Check Out</Text>
                </TouchableHighlight>
              </Form>
            </View>

          </View>
          {/* ============================== END ORDER LIST BOX ============================== */}

          {/* ============================== PRODUCT BOX ============================== */}
          <View style={styles.productBox}>
            <Tabs tabContainerStyle={{ height: 100 }}>
              <Tab heading={
                <TabHeading>
                  <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                    <Image
                      source={require('./img/coffee.png')}
                    />
                    <Text style={{ fontSize: 20, textAlign: 'center' }}>Coffee</Text>
                  </View>
                </TabHeading>}>
                <Tab1

                  //STATE
                  coffeeData={this.state.coffeeData}
                  showOptionModal={this.showOptionModal}
                  modalOption={this.state.modalOption}

                  //FUNCTION
                  shot={this.state.shot}
                  addShot={this.addShot}
                  reduceShot={this.reduceShot}
                  getType={this.getType}
                  getLevel={this.getLevel}
                  getSweet={this.getSweet}
                  getMilk={this.getMilk}
                  getTopping={this.getTopping}
                  getSize={this.getSize}
                />
              </Tab>

              <Tab heading={
                <TabHeading>
                  <View style={{ flexDirection: 'column', }}>
                    <Image
                      source={require('./img/tea.png')}
                    />
                    <Text style={{ fontSize: 20, textAlign: 'center' }}>Tea</Text>
                  </View>
                </TabHeading>}>
                <Tab2
                  //STATE
                  teaData={this.state.teaData}
                  modalOption={this.state.modalOption}
                  //FUNCTION
                  showOptionModal={this.showOptionModal}
                  getType={this.getType}
                  getSweet={this.getSweet}
                  getSize={this.getSize}
                />
              </Tab>

              <Tab heading={
                <TabHeading>
                  <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                    <Image
                      source={require('./img/soda.png')}
                    />
                    <Text style={{ fontSize: 20, textAlign: 'center' }}>Soda</Text>
                  </View>
                </TabHeading>}>
                <Tab3
                  //STATE
                  sodaData={this.state.sodaData}
                  modalOption={this.state.modalOption}
                  //FUNCTION
                  showOptionModal={this.showOptionModal}
                  getType={this.getType}
                  getSweet={this.getSweet}
                  getSize={this.getSize}
                />
              </Tab>

              <Tab heading={
                <TabHeading>
                  <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                    <Image
                      source={require('./img/cocoa.png')}
                    />
                    <Text style={{ fontSize: 20, textAlign: 'center' }}>Cocoa</Text>
                  </View>
                </TabHeading>}>
                <Tab4
                  //STATE
                  cocoaData={this.state.cocoaData}
                  modalOption={this.state.modalOption}
                  //FUNCTION
                  showOptionModal={this.showOptionModal}
                  getType={this.getType}
                  getSweet={this.getSweet}
                  getSize={this.getSize}
                />
              </Tab>

              <Tab heading={
                <TabHeading>
                  <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                    <Image
                      source={require('./img/milk.png')}
                    />
                    <Text style={{ fontSize: 20, textAlign: 'center' }}>Milk</Text>
                  </View>
                </TabHeading>}>
                <Tab5

                  //STATE
                  milkData={this.state.milkData}
                  modalOption={this.state.modalOption}
                  //FUNCTION
                  showOptionModal={this.showOptionModal}
                  getType={this.getType}
                  getSweet={this.getSweet}
                  getSize={this.getSize}
                />
              </Tab>

              <Tab heading={
                <TabHeading>
                  <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                    <Image
                      source={require('./img/bakery.png')}
                    />
                    <Text style={{ fontSize: 20, textAlign: 'center' }}>Bakery</Text>
                  </View>
                </TabHeading>}>
                <Tab6
                  //STATE
                  bakeryData={this.state.bakeryData}
                  modalOption={this.state.modalOption}
                  showOptionModal={this.showOptionModal}


                //FUNCTION
                />
              </Tab>
            </Tabs>
          </View>
          {/* ============================== END PRODUCT BOX ============================== */}

        </View>
        {/* ============================== END SALE BOX ============================== */}
      </View>
      // ============================== END MAIN BOX ============================== 
    } else {

      return <Logout></Logout>
    }
  }

  render() {


    return (
      // < Provider store={store} >
      this.showComponent()
      // </Provider>
    );
  }
}

//   render() {
//     console.log(this.state.orderList);
//     return (
//       // ============================== MAIN BOX ==============================
//       <View style={styles.mainBox}>
//         <Header style={{ backgroundColor: '#29ab87' }}>
//           <Left>
//             <Icon name="ios-menu" onPress={() => this.props.navigation.openDrawer()} />
//           </Left>
//           <Right>
//             <Icon name="lock" onPress={() => this.checked()} />
//           </Right>
//           {/* <Text style={{ fontSize: 18 }}>Sale</Text> */}
//         </Header>
//         {/* ============================== SALE BOX ============================== */}
//         <View style={styles.saleBox}>
//           {/* ============================== ORDER LIST BOX ============================== */}
//           <View style={styles.orderListBox}>
//             <Modal
//               animationType="slide"
//               transparent={false}
//               visible={this.state.modalCheckout}
//               onRequestClose={() => {
//                 Alert.alert('Modal has been closed.');
//               }}>
//               {/* ============================== BILL ============================== */}
//               <View style={{ flex: 1, marginTop: 22, flexDirection: 'row', }}>
//                 <View style={{ flex: 0.5, marginTop: 22, flexDirection: 'column', }}>
//                   <View style={{
//                     backgroundColor: '#f1f1ff',
//                     shadowOffset: {
//                       width: 5,
//                       height: 5,
//                     },
//                     shadowColor: 'black',
//                     shadowOpacity: 1.0,
//                     width: 400,
//                     height: 670,
//                     marginLeft: 50,
//                     marginTop: 20
//                   }}>
//                     <View style={{ marginTop: 20, marginLeft: 10 }}>
//                       <Text style={{ fontSize: 20, marginBottom: 5 }}> bill No: {this.state.billNo} </Text>
//                       <Text style={{ fontSize: 20, marginBottom: 5 }}> Date: {this.state.date} </Text>
//                       <Text style={{ fontSize: 20, marginBottom: 10 }}> By: {this.state.by} </Text>
//                       <Text style={{ fontSize: 20, marginBottom: 10 }}> Customer: {this.state.customer} </Text>
//                     </View>

//                     <View style={styles.rowOrderView}>
//                       <Text style={{ fontSize: 20 }}>Qty</Text>
//                       <Text style={{ fontSize: 20, marginLeft: 20 }}>Description</Text>
//                       <View style={styles.itemInOrder}>
//                         <Text style={{ fontSize: 20, marginLeft: 15 }}>Amount(B.)</Text>
//                       </View>
//                     </View>

//                     <ScrollView keyExtractor={(item, index) => index}>
//                       {
//                         this.state.orderList.map((item, index) => (
//                           <View style={styles.rowOrderView} key={index}>

//                             <Text style={{ fontSize: 20, marginLeft: 12 }}>{item.numPiece}</Text>
//                             <Text style={{ fontSize: 20, marginLeft: 30 }}>{item.orderItem.name}</Text>

//                             <View style={styles.itemInOrder}>
//                               <Text style={{ fontSize: 20 }}>{item.orderItem.price}</Text>
//                             </View>
//                           </View>


//                         ))
//                       }
//                     </ScrollView>
//                     <View style={{ flexDirection: 'row' }}>
//                       <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}> Total : </Text>
//                       <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}>{this.state.total}</Text>
//                     </View>

//                     <View style={{ flexDirection: 'row' }}>
//                       <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}> Discount : </Text>
//                       <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}>{this.state.discount}</Text>
//                     </View>

//                     <View style={{ flexDirection: 'row' }}>
//                       <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}> Cash : </Text>
//                       <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}>{this.state.cash}</Text>
//                     </View>
//                     <View style={{ flexDirection: 'row' }}>
//                       <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}> Change : </Text>
//                       <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 10 }}>{this.state.change}</Text>

//                     </View>

//                     <View style={{ flexDirection: 'row' }}>
//                       <Text style={{ fontSize: 50 }}> Change     </Text>
//                       <Text style={{ fontSize: 50, marginBottom: 10, marginLeft: 10 }}>{this.state.change}.</Text>
//                     </View>

//                   </View>
//                 </View>
//                 <View style={{ flex: 0.5, marginTop: 43, flexDirection: 'column', marginRight: 20 }}>
//                   <View style={{ flex: 0.2, flexDirection: 'row', justifyContent: 'flex-end', backgroundColor: '#f1f1ff' }}>
//                     <Text style={{ fontSize: 100, marginRight: 5 }}>{this.state.total}.</Text>
//                   </View>
//                   <View style={{ flex: 0.25, marginTop: 40, flexDirection: 'row', justifyContent: 'center' }}>
//                     <Text style={{ fontSize: 60, marginRight: 15, }}>Cash: </Text>
//                     <TextInput
//                       keyboardType={'number-pad'}
//                       style={{ height: 80, marginLeft: 5, padding: 8, fontSize: 80 }}
//                       placeholder={`${this.state.cash}`}
//                       onChangeText={(text) => {
//                         let t = this.state.total
//                         let sum = text - t

//                         this.setState({
//                           cash: text,
//                           change: sum
//                         })
//                       }
//                       }
//                     // value={`${this.state.cash}`}

//                     />
//                   </View>

//                   <View style={{ flex: 0.5, flexDirection: 'column', justifyContent: 'center', marginBottom: 30 }}>

//                     <View style={{ flex: 0.25, flexDirection: 'row', justifyContent: 'center' }}>


//                       <View style={{ flex: 0.3, backgroundColor: '#f1f1ff', justifyContent: 'center', marginLeft: 5, }}>
//                         <TouchableHighlight>
//                           <Text style={{ fontSize: 20, textAlign: 'center', color: 'black' }}>50</Text>
//                         </TouchableHighlight>
//                       </View>



//                       <View style={{ flex: 0.3, backgroundColor: '#f1f1ff', justifyContent: 'center', marginLeft: 5, }}>
//                         <TouchableHighlight>
//                           <Text style={{ fontSize: 20, textAlign: 'center', color: 'black' }}>100</Text>
//                         </TouchableHighlight>
//                       </View>



//                       <View style={{ flex: 0.3, backgroundColor: '#f1f1ff', justifyContent: 'center', marginLeft: 5, }}>
//                         <TouchableHighlight>
//                           <Text style={{ fontSize: 20, textAlign: 'center', color: 'black' }}>500</Text>
//                         </TouchableHighlight>
//                       </View>



//                       <View style={{ flex: 0.3, backgroundColor: '#f1f1ff', justifyContent: 'center', marginLeft: 5, }}>
//                         <TouchableHighlight>
//                           <Text style={{ fontSize: 20, textAlign: 'center', color: 'black' }}>1000</Text>
//                         </TouchableHighlight>
//                       </View>


//                     </View>
//                     <View style={{ flex: 0.25, flexDirection: 'row', justifyContent: 'center', marginTop: 40, }}>


//                       <Text style={{ fontSize: 25 }}>USE POINT</Text>

//                       <View style={{ marginLeft: 40 }}>
//                         <Text style={{ fontSize: 25 }}>PROMOTION</Text>
//                       </View>

//                     </View>




//                     <View style={{ flex: 0.5, backgroundColor: '#f1f1ff', justifyContent: 'center' }}>
//                       <TouchableHighlight>
//                         <Icon style={{ fontSize: 40, textAlign: 'center' }} name="ios-print" />
//                       </TouchableHighlight>
//                       <TouchableHighlight>
//                         <Text style={{ fontSize: 20, textAlign: 'center', color: 'black' }}>Print bill</Text>
//                       </TouchableHighlight>
//                     </View>

//                   </View>


//                   <View style={{ flex: 0.15, justifyContent: 'center', flexDirection: 'row', marginBottom: 30 }}>
//                     <View style={{ flex: 0.5, backgroundColor: '#ed1c24', justifyContent: 'center', }}>
//                       <TouchableHighlight
//                         onPress={() => {
//                           this.showCheckoutModal(!this.state.modalCheckout);
//                         }}>
//                         <Text style={{ fontSize: 30, textAlign: 'center', color: 'white' }}>CANCLE</Text>

//                       </TouchableHighlight>
//                     </View>
//                     <View style={{ flex: 0.5, backgroundColor: '#ffd500', justifyContent: 'center', marginLeft: 5 }}>
//                       <TouchableHighlight
//                         onPress={() => {
//                           this.pay(!this.state.modalCheckout);
//                         }}>
//                         <Text style={{ fontSize: 30, textAlign: 'center', color: 'white' }}>PAY</Text>

//                       </TouchableHighlight>
//                     </View>
//                   </View>
//                 </View>
//               </View>
//               {/* ============================== END BILL ============================== */}
//             </Modal>



//             <View style={{ flex: 0.2, flexDirection: 'column' }}>
//               <Form style={{ marginTop: 30, flex: 1, }}>
//                 <View style={{ marginTop: 5, flex: 0.6, flexDirection: 'row', marginLeft: 10, justifyContent: 'center' }}>
//                   <Icon style={{ fontSize: 40 }} name="ios-contact" />
//                   <TextInput
//                     style={{ height: 40, marginLeft: 5, padding: 8, fontSize: 25 }}
//                     placeholder="phone number"
//                     keyboardType={'number-pad'}
//                   />
//                 </View>

//               </Form>
//             </View>

//             <View style={{ flex: 0.7, backgroundColor: '#FFFFFF' }}>
//               <Button title="Order List" />
//               <ScrollView keyExtractor={(item, index) => index}>
//                 {

//                   this.state.orderList.map((item, index) => (

//                     <View style={styles.rowOrderView} key={index}>

//                       <Text style={styles.textItemOrder}>{item.orderItem.name}</Text>
//                       <View style={styles.itemInOrder}>
//                         <TouchableHighlight onPress={() => this.reducePiece(index)}>
//                           <Icon style={styles.iconRemove} name="ios-remove" />
//                         </TouchableHighlight>
//                         <Text style={styles.numberPieces}>{item.numPiece}</Text>
//                         <TouchableHighlight onPress={() => this.addPiece(index)}>
//                           <Icon style={styles.iconAdd} name="ios-add" />
//                         </TouchableHighlight>
//                         <TouchableHighlight onPress={() => this.deleteItem(index)}>
//                           <Icon style={styles.iconTrash} name="ios-trash" />
//                         </TouchableHighlight>
//                       </View>

//                     </View>


//                   ))
//                 }
//               </ScrollView>
//             </View>

//             <View style={{ flex: 0.1, flexDirection: 'column', backgroundColor: '#29ab87', justifyContent: 'center', marginTop: 15 }}>
//               <Form style={{ backgroundColor: '#29ab87', }}>
//                 {/* <Button title={this.state.buttonCheckOut} /> */}
//                 <TouchableHighlight onPress={() => { this.showCheckoutModal(true); }}>
//                   <Text style={{ color: '#ffffff', fontSize: 25, textAlign: 'center' }}>Check Out</Text>
//                 </TouchableHighlight>
//               </Form>
//             </View>

//           </View>
//           {/* ============================== END ORDER LIST BOX ============================== */}

//           {/* ============================== PRODUCT BOX ============================== */}
//           <View style={styles.productBox}>
//             <Tabs tabContainerStyle={{ height: 100 }}>
//               <Tab heading={
//                 <TabHeading>
//                   <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
//                     <Image
//                       source={require('./img/coffee.png')}
//                     />
//                     <Text style={{ fontSize: 20, textAlign: 'center' }}>Coffee</Text>
//                   </View>
//                 </TabHeading>}>
//                 <Tab1

//                   //STATE
//                   coffeeData={this.state.coffeeData}
//                   showOptionModal={this.showOptionModal}
//                   modalOption={this.state.modalOption}

//                   //FUNCTION
//                   shot={this.state.shot}
//                   addShot={this.addShot}
//                   reduceShot={this.reduceShot}
//                   getType={this.getType}
//                   getLevel={this.getLevel}
//                   getSweet={this.getSweet}
//                   getMilk={this.getMilk}
//                   getTopping={this.getTopping}
//                   getSize={this.getSize}
//                 />
//               </Tab>

//               <Tab heading={
//                 <TabHeading>
//                   <View style={{ flexDirection: 'column', }}>
//                     <Image
//                       source={require('./img/tea.png')}
//                     />
//                     <Text style={{ fontSize: 20, textAlign: 'center' }}>Tea</Text>
//                   </View>
//                 </TabHeading>}>
//                 <Tab2
//                   //STATE
//                   teaData={this.state.teaData}
//                   modalOption={this.state.modalOption}
//                   //FUNCTION
//                   showOptionModal={this.showOptionModal}
//                   getType={this.getType}
//                   getSweet={this.getSweet}
//                   getSize={this.getSize}
//                 />
//               </Tab>

//               <Tab heading={
//                 <TabHeading>
//                   <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
//                     <Image
//                       source={require('./img/soda.png')}
//                     />
//                     <Text style={{ fontSize: 20, textAlign: 'center' }}>Soda</Text>
//                   </View>
//                 </TabHeading>}>
//                 <Tab3
//                   //STATE
//                   sodaData={this.state.sodaData}
//                   modalOption={this.state.modalOption}
//                   //FUNCTION
//                   showOptionModal={this.showOptionModal}
//                   getType={this.getType}
//                   getSweet={this.getSweet}
//                   getSize={this.getSize}
//                 />
//               </Tab>

//               <Tab heading={
//                 <TabHeading>
//                   <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
//                     <Image
//                       source={require('./img/cocoa.png')}
//                     />
//                     <Text style={{ fontSize: 20, textAlign: 'center' }}>Cocoa</Text>
//                   </View>
//                 </TabHeading>}>
//                 <Tab4
//                   //STATE
//                   cocoaData={this.state.cocoaData}
//                   modalOption={this.state.modalOption}
//                   //FUNCTION
//                   showOptionModal={this.showOptionModal}
//                   getType={this.getType}
//                   getSweet={this.getSweet}
//                   getSize={this.getSize}
//                 />
//               </Tab>

//               <Tab heading={
//                 <TabHeading>
//                   <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
//                     <Image
//                       source={require('./img/milk.png')}
//                     />
//                     <Text style={{ fontSize: 20, textAlign: 'center' }}>Milk</Text>
//                   </View>
//                 </TabHeading>}>
//                 <Tab5

//                   //STATE
//                   milkData={this.state.milkData}
//                   modalOption={this.state.modalOption}
//                   //FUNCTION
//                   showOptionModal={this.showOptionModal}
//                   getType={this.getType}
//                   getSweet={this.getSweet}
//                   getSize={this.getSize}
//                 />
//               </Tab>

//               <Tab heading={
//                 <TabHeading>
//                   <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
//                     <Image
//                       source={require('./img/bakery.png')}
//                     />
//                     <Text style={{ fontSize: 20, textAlign: 'center' }}>Bakery</Text>
//                   </View>
//                 </TabHeading>}>
//                 <Tab6
//                   //STATE
//                   bakeryData={this.state.bakeryData}
//                   modalOption={this.state.modalOption}
//                   showOptionModal={this.showOptionModal}


//                 //FUNCTION
//                 />
//               </Tab>
//             </Tabs>
//           </View>
//           {/* ============================== END PRODUCT BOX ============================== */}

//         </View>
//         {/* ============================== END SALE BOX ============================== */}
//       </View>
//       // ============================== END MAIN BOX ============================== 
//     );
//   }
// }


const mapStateToProps = ({ reducers }) => {
  let { coffeeData, noncoffeeData, teaData, sodaData, cocoaData, milkData, bakeryData, orderList } = reducers
  return {
    coffeeData, noncoffeeData, teaData, sodaData, cocoaData, milkData, bakeryData, orderList
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addItemAction: payload => dispatch(addItem(payload)),
    addBillAction: payload => dispatch(addBill(payload)),
    updateListAction: payload => dispatch(updateList(payload)),
    setCoffeeDataAction: payload => dispatch(setCoffeeData(payload)),
    setTeaDataAction: payload => dispatch(setTeaData(payload)),
    setNonCoffeeDataAction: payload => dispatch(setNonCoffeeData(payload)),
    setBakeryDataAction: payload => dispatch(setBakeryData(payload)),
    clearItemAction: () => dispatch(clearItem()),
  }
}

export default
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)