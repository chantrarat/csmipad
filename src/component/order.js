
import React from 'react';
import { StyleSheet, Text, View, FlatList, Dimensions, TouchableHighlight, Modal, Button, ScrollView, Switch } from 'react-native';
import {
    Container,
    Header,
    Item,
    Input,
    Icon,
    Button as B,
    Text as T,
    Content,
    CheckBox,
    Body,
    Left,
} from 'native-base'
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';

import { connect } from 'react-redux'
import { addBill } from '../Action'
import DatePicker from 'react-native-datepicker'

import * as firebase from 'firebase';
import Firebase from '../Firebase/firebase'

if (!firebase.apps.length) {
    firebase.initializeApp(Firebase.FirebaseConfig);
}



const formatData = (data, numColumns) => {
    const numberOfFullRows = Math.floor(data.length / numColumns);

    let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
    while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
        data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
        numberOfElementsLastRow++;
    }

    return data;
};

const numColumns = 5;
export class App extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            search: '',
            modalCheckout: false,
            order: [],
            showBill: {},
            showOrderlist: [],
            orderSuccess: false,
            date: new Date(),
        }
        console.log(this.props)
    }
    // componentWillMount() {
    //     console.log(this.props.bill);
    //     if (this.props.bill.length != 0) {
    //         this.setState({
    //             order: [...this.props.bill]
    //         })
    //     }
    // }
    componentDidMount() {
        let d = new Date().getDate()
        let m = new Date().getMonth() + 1
        let y = new Date().getFullYear()

        firebase.database().ref('/Order/' + y + '/' + m + '/' + d).once('value', (data) => {

            let d = data.toJSON()
            let waitD = Object.keys(d).length

            let j = 1
            let o = []
            Object.keys(d).forEach((key, index) => {
                o.push({
                    id: j++,
                    billNo: d[key].billNo,
                    by: d[key].by,
                    cash: d[key].cash,
                    change: d[key].change,
                    customer: d[key].customer,
                    date: d[key].date,
                    discount: d[key].discount,
                    orderList: d[key].orderList,
                    printBill: d[key].printBill,
                    time: d[key].time,
                    total: d[key].total
                })
                if (waitD == index + 1) {
                    this.setState({
                        order: o
                    })
                }
            })


        })
    }

    updateData (date) {
        let splitDate = date.split('-')
        
        // let d = d.getDate()
        let d = parseInt(splitDate[0])
        let m = parseInt(splitDate[1])
        let y = parseInt(splitDate[2])
        
        // // firebase.database().ref('/').on('value', (data) => {
        firebase.database().ref('/Order/' + y + '/' + m + '/' + d).once('value', (data) => {

            let d = data.toJSON()
            let waitD = Object.keys(d).length

            let j = 1
            let o = []
            Object.keys(d).forEach((key, index) => {
                o.push({
                    id: j++,
                    billNo: d[key].billNo,
                    by: d[key].by,
                    cash: d[key].cash,
                    change: d[key].change,
                    customer: d[key].customer,
                    date: d[key].date,
                    discount: d[key].discount,
                    orderList: d[key].orderList,
                    printBill: d[key].printBill,
                    time: d[key].time,
                    total: d[key].total
                })
                if (waitD == index + 1) {
                    this.setState({
                        order: o
                    })
                }
            })


        })
    }


    // componentWillUpdate(nextProps) {
    //     console.log(nextProps)
    //     console.log(this.props.bill);
    // }
    // componentDidUpdate() {
    //     // if (this.props.bill.length != 0) {             
    //     //     this.setState({
    //     //         order: [...this.props.bill]
    //     //     })
    //     // }
    //     console.log(this.props.bill);
    // }
    // componentWillReceiveProps(nextProps) {
    //     console.log(nextProps.bill);

    // }

    showCheckoutModal = (visible, id = -1) => {
        console.log("83", id)
        if (id === -1) {
            this.setState({
                modalCheckout: visible,
            });
        } else {
            let bill = { ...this.state.order.find(item => id === item.billNo) }
            let or = Object.values(bill.orderList)
            console.log("Test", bill, or)

            this.setState({
                //find all data
                showBill: bill,
                //find some data .
                showOrderlist: or,
                modalCheckout: visible,
            });
        }

    }
    updateSearch = search => {
        this.setState({ search });
    }

    clearSearsh = () => {
        let { search } = this.state
        search = ''
        this.setState({ search });
    }

    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <Icon name="paper" style={{ fontSize: 24, color: tintColor }} />
        )
    }

    onChangeSwitch = (value) => {
        this.setState({
            orderSuccess: value
        })
    }


    renderItem = ({ item, index }) => {
        console.log(item)
        if (item.empty === true) {
            return <View style={[styles.item, styles.itemInvisible]} />;
        }
        return (

            <View style={styles.item} >
                <Text style={styles.itemText}>Order : {item.billNo}</Text>
                <Text style={styles.itemText}>Staff : {item.by}</Text>
                <Text style={styles.itemText}>Amount : {item.total}</Text>
                <Text style={styles.itemText}>Date : {item.date}</Text>
                <Text style={styles.itemText}>Time : {item.time}</Text>

                <View style={{ marginTop: 10, }}>

                    <Button
                        onPress={() => { this.showCheckoutModal(true, item.billNo) }}
                        title='Open'
                    />
                </View>
            </View>

        );

    };


    render() {
        const { search, order, showBill, } = this.state;
        console.log("139", this.state.showBill);
        console.log("140", this.state.showOrderlist);

        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalCheckout}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}>

                    <View style={{ flex: 1, flexDirection: 'column', marginTop: 50 }}>
                        {/* <Text style={{ fontSize: 100 }}>Order</Text> */}

                        {/* <CheckBox checked={false} color="green" /> */}
                        <Body>
                            <View style={{flexDirection: 'row'}}>

                            {/* <View style={{ flexDirection: 'row' }} > */}
                            <Text style={{ fontSize: 20 }} >Bill No : {showBill.billNo}      </Text>
                            {/* </View> */}

                            {/* <View style={{ flexDirection: 'row', marginLeft: 20 }} > */}
                            <Text style={{ fontSize: 20 }}>Date : {showBill.date} </Text>
                            {/* <Text style={{ fontSize: 20 }}>Time : {showBill.time} </Text> */}
                            {/* </View> */}

                            {/* <View style={{ flexDirection: 'row', marginLeft: 20 }} > */}
                            {/* <Text style={{ fontSize: 20 }}>By : {showBill.by}</Text> */}
                            {/* <Text style={{ fontSize: 20 }}>Customer : {showBill.customer}</Text> */}
                            {/* </View> */}

                            {/* <View style={{ flexDirection: 'row', marginLeft: 20 }} > */}
                            {/* <Text style={{ fontSize: 20 }}>Total : {showBill.total}</Text> */}
                            {/* <Text style={{ fontSize: 20 }}>Discount : {showBill.discount}</Text> */}
                            {/* </View> */}

                            {/* <View style={{ flexDirection: 'row' }} > */}
                            {/* <Text style={{ fontSize: 20 }}>Cash : {showBill.cash}</Text> */}
                            {/* <Text style={{ fontSize: 20 }}>Change : {showBill.change}</Text> */}
                            {/* </View> */}

                            </View>



                            <ScrollView keyExtractor={(item, index) => index}>
                                {
                                    this.state.showOrderlist.map((item, index) => (
                                        <View style={styles.rowOrderView} key={index}>
                                            <View>
                                                <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }} >
                                                    <Text style={{ fontSize: 30, marginLeft: 12 }}>{item.orderItem.name}</Text>
                                                </View>

                                                <View style={{ flexDirection: 'row', justifyContent: "center" }}>

                                                    <View style={{ width: 110, height: 40, backgroundColor: '#c08457', marginLeft: 5, flexDirection: 'row', justifyContent: "center" }} >
                                                        <Text style={{ fontSize: 20, textAlign: 'center', marginTop: 5, color: 'white' }}>{item.type}</Text>
                                                    </View>

                                                    <View style={{ width: 110, height: 40, backgroundColor: '#c08457', marginLeft: 5, flexDirection: 'row', justifyContent: "center" }}>
                                                        <Text style={{ fontSize: 20, textAlign: 'center', marginTop: 5, color: 'white' }}>{item.level}</Text>
                                                    </View>


                                                    <View style={{ width: 110, height: 40, backgroundColor: '#c08457', marginLeft: 5, flexDirection: 'row', justifyContent: "center" }}>
                                                        <Text style={{ fontSize: 20, textAlign: 'center', marginTop: 5, color: 'white' }}>{item.numShot}</Text>
                                                    </View>


                                                    <View style={{ width: 110, height: 40, backgroundColor: '#c08457', marginLeft: 5, flexDirection: 'row', justifyContent: "center" }}>
                                                        <Text style={{ fontSize: 20, textAlign: 'center', marginTop: 5, color: 'white' }}>{item.sweet}</Text>
                                                    </View>


                                                    <View style={{ width: 110, height: 40, backgroundColor: '#c08457', marginLeft: 5, flexDirection: 'row', justifyContent: "center" }}>
                                                        <Text style={{ fontSize: 20, textAlign: 'center', marginTop: 5, color: 'white' }}>{item.milk}</Text>
                                                    </View>


                                                    <View style={{ width: 110, height: 40, backgroundColor: '#c08457', marginLeft: 5, flexDirection: 'row', justifyContent: "center" }}>
                                                        <Text style={{ fontSize: 20, textAlign: 'center', marginTop: 5, color: 'white' }}>{item.topping}</Text>
                                                    </View>


                                                    <View style={{ width: 110, height: 40, backgroundColor: '#c08457', marginLeft: 5, flexDirection: 'row', justifyContent: "center" }}>
                                                        <Text style={{ fontSize: 20, textAlign: 'center', marginTop: 5, color: 'white' }}>{item.size}</Text>
                                                    </View>

                                                </View>

                                                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginTop: 10 }} >
                                                    <Text style={{ fontSize: 20, marginLeft: 30 }}>Total  : </Text>
                                                    <Text style={{ fontSize: 20, marginLeft: 20 }}>{item.numPiece}  cup.</Text>
                                                    {/* <View style={{ flexDirection: 'row', marginLeft: 20 }} >
                                                        <Switch onValueChange={this.onChangeSwitch} value={this.state.orderSuccess} />
                                                        <Text style={{ fontSize: 20, textAlign: 'center', color: 'black', marginLeft: 5, marginTop: 2 }}>Success</Text>
                                                    </View> */}
                                                </View>

                                            </View>


                                        </View>


                                    ))
                                }

                            </ScrollView>


                            {/* <View style={{ flexDirection: 'row' }} >
                                <Switch onValueChange={this.onChangeSwitch} value={this.state.orderSuccess} />
                                <Text style={{ fontSize: 20, textAlign: 'center', color: 'black', marginLeft: 5, marginTop: 2 }}>Success</Text>
                            </View> */}


                        </Body>
                        <TouchableHighlight
                            onPress={() => {
                                this.showCheckoutModal(!this.state.modalCheckout, -1);
                            }}>
                            <Text style={{ fontSize: 40, textAlign: 'center', color: 'red' }}>Close</Text>
                        </TouchableHighlight>
                    </View>
                </Modal>

                <View style={{ justifyContent: 'flex-end' }}>
                    <Header style={{ backgroundColor: '#29ab87' }}>
                        <Left>
                            <Icon name="ios-menu" onPress={() => this.props.navigation.openDrawer()} />
                        </Left>
                    </Header>
                </View>

                <DatePicker
                    style={{ width: 200, marginLeft: 400, marginTop: 10, marginBottom: 10 }}
                    date={this.state.date}
                    mode="date"
                    placeholder="select date"
                    format="DD-MM-YYYY"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 4,
                            marginLeft: 0
                        },
                        dateInput: {
                            marginLeft: 36
                        }
                        // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => {
                        this.updateData(date)
                        this.setState({ date: date })


                    }}
                />

                <FlatList
                    data={formatData(order, numColumns)}
                    style={styles.container}
                    renderItem={this.renderItem}
                    numColumns={numColumns}

                    keyExtractor={(item, index) => index}
                />

            </View>

        );
    }
}

const mapStateToProps = ({ reducers }) => {
    let { bill } = reducers
    return {
        bill
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addBillAction: payload => dispatch(addBill(payload)),
    }
}

export default
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(App)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginVertical: 0,
        backgroundColor: '#BEBEBE'
    },
    item: {
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        margin: 1,
        height: Dimensions.get('window').width / numColumns, // approximate a square
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },
    itemText: {
        color: '#181818',
        fontSize: 20
    },
    rowOrderView: {
        paddingRight: 20,
        paddingTop: 20,
        paddingBottom: 20,
        marginLeft: 20,
        flexDirection: 'row',
    },

    itemInOrder: {
        marginLeft: 'auto',
        flexDirection: 'row',

    },
});

