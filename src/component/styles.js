
import { StyleSheet } from 'react-native';
var styles = StyleSheet.create({
    
    mainBox: {
        flex: 1, 
        flexDirection: 'column', 
        justifyContent: 'center', 
        
    },
    
    
    //-------------sales.js
    saleBox: {
        flex: 8, 
        flexDirection: 'row', 
        
    },
    orderListBox: {
        flex: 0.35, 
        backgroundColor: '#ffffff'
    },
    productBox: {
        flex: 0.65, 
    },
    rowOrderView:{
        paddingRight: 20,
        paddingTop: 20,
        paddingBottom: 20,
        marginLeft: 20,
        flexDirection: 'row',
      },
      itemInOrder:{
        marginLeft: 'auto', 
        flexDirection: 'row',
        
      },
      textItemOrder:{
        fontSize: 20,
        marginTop: 5
      },
      iconRemove:{
        fontSize: 30, 
        marginTop: 5,
        marginLeft: 10
      },
      numberPieces:{
        fontSize: 30, 
        marginLeft: 10,
      },
      iconAdd:{
        fontSize: 30, 
        marginTop: 5,
        marginLeft: 10
      },
      iconTrash:{
        fontSize: 30, 
        marginTop: 5,
        marginLeft: 50
      },
    //-------------END sales.js

    //-------------App.js
    Top: {
        flex: 0.1, 
        flexDirection: 'row', 
        backgroundColor: '#ffffff'
    },
    Content: {
        flex: 0.9, 
        flexDirection: 'row', 
        backgroundColor: '#100305'
    },
    //-------------END App.js
    
    //------------- tabCoffee.js
    main: {
      flex: 1, 
      justifyContent: 'center', 
      
  },
    imageThumbnail: {
      justifyContent: 'center',
      alignItems: 'center',
      // width: 150,
      height: 100,
    },

    imagePromotion: {
      justifyContent: 'center',
      alignItems: 'center',
      width: 200,
      height: 300,
    },
    //------------- END tabCoffee.js

    rowBestView:{
      paddingRight: 20,
      paddingBottom: 20,
      paddingBottom: 20,
      flexDirection: 'row',
    },
    iconBest:{
      fontSize: 20, 
      marginTop: 5,
      marginLeft: 70
    },

  });
  export default styles;