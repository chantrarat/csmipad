import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  Button as B,
  TouchableHighlight,
} from 'react-native';
import {
  Icon,
  Header,
  Left,
  Content,
  CheckBox,
  Card,
  CardItem,
  List, ListItem,
} from 'native-base';

import styles from './styles';
import { Col, Row, Grid } from "react-native-easy-grid";
import { TextInput, Button, Searchbar } from 'react-native-paper';
// import RNFetchBlob from 'rn-fetch-blob'
import * as firebase from 'firebase';
import Firebase from '../Firebase/firebase'

if (!firebase.apps.length) {
  firebase.initializeApp(Firebase.FirebaseConfig);
}





export default class saleScreen extends Component {
  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      basic: true,
      lastname: '',
      nickname: '',
      query: '',
      data: [],
      test: "",

    };
  }

  static navigationOptions = {
    drawerIcon: ({ tintColor }) => (
      <Icon name="people" style={{ fontSize: 24, color: tintColor }} />
    )
  }

  componentDidMount() {
    firebase.database().ref('/').on('value', (data) => {
      let c = data.toJSON().Customer
      let waitC = Object.keys(c).length
      console.log("wait", waitC);
      let i = 1
      let d = []
      Object.keys(c).forEach((key, index) => {
        d.push({ key: key, id: i++, firstName: c[key].firstName, lastName: c[key].lastName, point: c[key].point, level: c[key].level })
        if (waitC == index + 1) {
          this.setState({
            data: d
          })
        }
      })
    })
  }

  search() {
    firebase.database().ref('/Customer')
      .orderByChild('firstName')
      .equalTo(this.state.query).once('value', (data) => {
        this.setState({
          test: data
        })
      })
  }


  render() {
    const { query } = this.state;

    return (
      <View style={{ flex: 1 }}>
        <Header style={{ backgroundColor: '#29ab87' }}>
          <Left>
            <Icon name="ios-menu" onPress={() => this.props.navigation.openDrawer()} />
          </Left>
        </Header>


        <Content contentContainerStyle={{ flex: 1 }}>
          <Grid style={{ backgroundColor: "#FFFFFF" }}>
            <Col style={{ marginTop: 10, marginRight: 100, marginLeft: 100 }}>
              {/* <Searchbar
                placeholder="Search"
                onChangeText={q => {
                  // this.search()
                  this.setState({ query: q });
                }}
                value={query}
              /> */}
              <Text style={{ fontSize: 30, justifyContent: 'center', }}>รายชื่อสมาชิก</Text>
              <ScrollView keyExtractor={(item, index) => index}>
                {

                  this.state.data.map((item, index) => (
                    <View style={{ backgroundColor: '#29ab87', margin: 3 }}>
                      <View style={styles.rowOrderView} key={index}>

                        <View style={{ flexDirection: 'column' }}>
                          <Text style={styles.textItemOrder}>{item.firstName}  {item.lastName}</Text>
                          <Text style={styles.textItemOrder}>Level : {item.level}</Text>
                        </View>


                        <View style={styles.itemInOrder}>
                          {/* <TouchableHighlight onPress={() => this.deleteItem(index, item.key)}> */}
                          <Text style={styles.textItemOrder}>Point : {item.point}   </Text>

                          {/* </TouchableHighlight> */}
                        </View>
                        {/* <View style={styles.itemInOrder}>
                        <TouchableHighlight onPress={() => this.deleteItem(index,item.key)}>
                          <Icon style={styles.iconTrash} name="ios-trash" />
                        </TouchableHighlight>
                      </View> */}
                      </View>


                    </View>


                  ))
                }
              </ScrollView>
            </Col>
          </Grid>
        </Content>
      </View>
    );
  }
}


