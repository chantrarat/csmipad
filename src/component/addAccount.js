
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    ListView,
    Button as B,
    TouchableHighlight,
    Alert,
} from 'react-native';
import {
    Icon,
    Header,
    Right,
    Left,
    Content,
    CheckBox,
    Card,
    CardItem,
    List, ListItem,
} from 'native-base';

import styles from './styles';
import Logout from '../../App'

import { Col, Row, Grid } from "react-native-easy-grid";
import { TextInput, Button, Searchbar } from 'react-native-paper';
import Firebase from '../Firebase/firebase'
import * as firebase from 'firebase';

if (!firebase.apps.length) {
    firebase.initializeApp(Firebase.FirebaseConfig);
}



export default class saleScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            basic: true,

            owner: false,
            staff: false,
            id: '',
            username: '',
            password: '',
            name: '',
            lname: '',
            license: '',
            statusLogin: false,
            keyStaff: '',
            member: [],
            keyMember: '',
            statusLogin: true,
        };
    }
    clear() {
        this.setState({
            owner: false,
            staff: false,
            id: '',
            username: '',
            password: '',
            name: '',
            lname: '',
            license: '',
        })
    }

    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <Icon name="person" style={{ fontSize: 24, color: tintColor }} />
        )
    }
    ownerPressed() {
        this.setState({ owner: true, staff: false })
    }
    staffPressed() {
        this.setState({ owner: false, staff: true })
    }

    componentDidMount() {
        firebase.database().ref('Account/').on('value', (data) => {
            let all = data.toJSON()
            let wait = Object.keys(all).length
            let i = 1
            let allMember = []
            Object.keys(all).forEach((key, index) => {
                allMember.push({
                    key: key,
                    id: all[key].id,
                    username: all[key].username,
                    password: all[key].password,
                    name: all[key].name,
                    lname: all[key].lastName,
                    license: all[key].license,
                    statusLogin: all[key].statusLogin
                })
                if (wait == index + 1) {
                    this.setState({
                        member: allMember
                    })
                }
            })
        })
    }
    deleteItem = (index, key) => {
        // console.log("STAFF", this.state.staff)
        // console.log("INDEX", index)
        // console.log("KEY", key)

        firebase.database().ref('Account/' + key).remove();


    }

    save = (k) => {

        let {
            id,
            username,
            password,
            name,
            lname,
            license,
            statusLogin,
            owner,
            staff
        } = this.state
        if (owner === false && staff === true) {
            firebase.database().ref('Account/').push({
                id,
                username,
                password,
                name,
                lname,
                license: 'Staff',
                statusLogin
            }).then((data) => {
                //success callback
                console.log('data ', data)
            }).catch((error) => {
                //error callback
                console.log('error ', error)
            })
        } else {
            firebase.database().ref('Account/').push({
                id,
                username,
                password,
                name,
                lname,
                license: 'Owner',
                statusLogin
            }).then((data) => {
                //success callback
                console.log('data ', data)
            }).catch((error) => {
                //error callback
                console.log('error ', error)
            })

        }





        this.setState({
            owner: false,
            staff: false,
            username: '',
            password: '',
            name: '',
            lname: '',
            license: '',
            keyMember: '',
        });
    }

    // deleteItem = (index, key) => {

    //   firebase.database().ref('/Account' + '/' + this.state.uid + '/Staff' + key).remove();

    // }

    edit = (index, key) => {
        let { member } = this.state
        let memberEdit = member.find(member => member.key === key)
        // console.log("Pro", staffEdit)
        if (memberEdit.license === 'Owner') {
            this.setState({
                id: memberEdit.id,
                username: memberEdit.username,
                password: memberEdit.password,
                name: memberEdit.name,
                lname: memberEdit.lname,
                license: memberEdit.license,
                statusLogin: memberEdit.statusLogin,
                owner: true,
                staff: false,
                keyMember: key
            })
        } else {
            this.setState({
                id: memberEdit.id,
                username: memberEdit.username,
                password: memberEdit.password,
                name: memberEdit.name,
                lname: memberEdit.lname,
                license: memberEdit.license,
                statusLogin: memberEdit.statusLogin,
                owner: false,
                staff: true,
                keyMember: key
            })
        }

    }

    checked = () => {
        Alert.alert(
            'You want logout?',
            '',
            [
                // { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'OK', onPress: () => this.logout('ok') },
            ],
            { cancelable: false }
        )
    }

    logout = (value) => {
        firebase.database().ref('/Account')
            .once('value', (data) => {
                if (data.val() != null) {
                    let d = data.val()
                    let key = Object.keys(d)[0]
                    if (value === 'ok') {

                        firebase.database().ref().child('/Account/' + key).update(
                            {
                                statusLogin: false,
                            })
                        this.setState({
                            statusLogin: false,
                        })
                    } else {
                        this.setState({
                            statusLogin: true,
                        })
                    }
                } else {
                    console.log("null")
                }

            })
    }

    showComponent = () => {
        if (this.state.statusLogin) {
            return  <View style={{ flex: 1 }}>
            <Header style={{ backgroundColor: '#29ab87' }}>
                <Right>
                    <Icon name="lock" onPress={() => this.checked()} />
                </Right>
            </Header>
            <Content contentContainerStyle={{ flex: 1 }}>
                <Grid style={{ backgroundColor: "#FFFFFF" }}>
                    <Col >
                        <Row>
                            <Col style={{ width: 100 }}></Col>
                            {/* form */}
                            <Col style={{ marginTop: 10, marginTop: 80 }}>
                                <ScrollView>
                                    <TextInput
                                        style={{ marginBottom: 20 }}
                                        label='Username'
                                        mode='outlined'
                                        value={this.state.username}
                                        onChangeText={text => this.setState({ username: text })}
                                    />
                                    <TextInput
                                        style={{ marginBottom: 20 }}
                                        label='Password'
                                        mode='outlined'
                                        value={this.state.password}
                                        onChangeText={text => this.setState({ password: text })}
                                    />
                                    <TextInput
                                        style={{ marginBottom: 20 }}
                                        label='Name'
                                        mode='outlined'
                                        value={this.state.name}
                                        onChangeText={text => this.setState({ name: text })}
                                    />
                                    <TextInput
                                        style={{ marginBottom: 20 }}
                                        label='Last name'
                                        mode='outlined'
                                        value={this.state.lname}
                                        onChangeText={text => this.setState({ lname: text })}
                                    />

                                    <Card>
                                        <CardItem header>
                                            <Text>Please select license</Text>
                                        </CardItem>
                                        <CardItem body>
                                            <CheckBox checked={this.state.owner}
                                                onPress={() => this.ownerPressed()}
                                                style={{ marginRight: 20 }}
                                            />
                                            <Text>Owner</Text>
                                        </CardItem>
                                        <CardItem body>
                                            <CheckBox checked={this.state.staff}
                                                onPress={() => this.staffPressed()}
                                                style={{ marginRight: 20 }}
                                            />
                                            <Text>Staff</Text>
                                        </CardItem>
                                    </Card>
                                    <Row>
                                        <Col>
                                            <Button

                                                mode="outlined"
                                                onPress={() => this.clear()}
                                                color="#e74c3c"
                                                style={{ marginTop: 20, marginLeft: 20, marginRight: 20 }}
                                            >
                                                Clear
                </Button>
                                        </Col>
                                        <Col>
                                            <Button

                                                mode="outlined"
                                                onPress={() => this.save('')}
                                                color="#2ecc71"
                                                style={{ marginTop: 20, marginLeft: 20, marginRight: 20 }}
                                            >
                                                Save
                </Button>
                                        </Col>
                                    </Row>
                                </ScrollView>
                            </Col>
                        </Row>
                    </Col>

                    {/* listname */}
                    <Col style={{ marginTop: 70, marginRight: 100, marginLeft: 100 }}>
                        {/* <Searchbar
            placeholder="Search"
            onChangeText={query => { this.setState({ firstQuery: query }); }}
            value={firstQuery}
          /> */}

                        <ScrollView keyExtractor={(item, index) => index}>
                            {

                                this.state.member.map((item, index) => (

                                    <View style={styles.rowOrderView} key={index}>
                                        <View style={{ flexDirection: 'column' }}>
                                            <Text style={styles.textItemOrder}>{item.name}</Text>
                                            <Text style={styles.textItemOrder}>license : {item.license}</Text>
                                        </View>

                                        <View style={styles.itemInOrder}>
                                            <B
                                                onPress={() => this.edit(index, item.key)}
                                                title="Edit"
                                            />
                                            <TouchableHighlight
                                                onPress={() => this.deleteItem(index, item.key)}
                                            >
                                                <Icon style={styles.iconTrash} name="ios-trash" />
                                            </TouchableHighlight>
                                        </View>

                                    </View>


                                ))
                            }
                        </ScrollView>
                    </Col>

                </Grid>
            </Content>
        </View>
        } else {

            return <Logout></Logout>
        }
    }

    render() {
        const { firstQuery } = this.state;
        return (
            this.showComponent()
        );
    }
}







// //===============================ADD STAFF========================================
// import React, { Component } from 'react';
// import {
//     View,
//     FlatList,
//     Image,
//     Text,
//     TouchableHighlight,
//     Alert,
//     Modal,
//     ScrollView,
//     TextInput,
//     Button
// } from 'react-native';
// import Firebase from '../Firebase/firebase'
// import * as firebase from 'firebase';

// if (!firebase.apps.length) {
//     firebase.initializeApp(Firebase.FirebaseConfig);
// }

// export default class App extends Component {


//     constructor(props) {
//         super(props);
//         this.state = {
//             ID: '',
//             username: '',
//             password: '',
//             name: '',
//             lname: '',
//             license: '',
//             statusLogin: false,
//         }
//     }



//     addAccount = () => {

//         let {
//             ID,
//             username,
//             password,
//             name,
//             lname,
//             license,
//             statusLogin
//         } = this.state

//         firebase.database().ref('Account/').push({
//             ID,
//             username,
//             password,
//             name,
//             lname,
//             license,
//             statusLogin
//         }).then((data) => {
//             //success callback
//             console.log('data ', data)
//         }).catch((error) => {
//             //error callback
//             console.log('error ', error)
//         })

//         this.setState({
//             ID,
//             username,
//             password,
//             name,
//             lname,
//             license,
//             statusLogin
//         });
//     }

//     render() {
//         return (
//             <View style={{ backgroundColor: 'white', flex: 1 }}>

//                 <View style={coinDetailStyle.containerMain}>
//                     <View style={coinDetailStyle.containerInput}>
//                         <Text style={coinDetailStyle.textForm} >ID</Text>
//                         <TextInput style={coinDetailStyle.textInputEmail}
//                             autoCorrect={false}
//                             placeholder='ID'
//                             onChangeText={(text) => { this.setState({ ID: text }) }}
//                         ></TextInput>
//                     </View>
//                     <View style={coinDetailStyle.containerInput}>
//                         <Text style={coinDetailStyle.textForm} >Username</Text>
//                         <TextInput style={coinDetailStyle.textInputEmail}
//                             autoCorrect={false}
//                             placeholder='Username'
//                             onChangeText={(text) => { this.setState({ username: text }) }}
//                         ></TextInput>
//                     </View>
//                     <View style={coinDetailStyle.containerInput}>
//                         <Text style={coinDetailStyle.textForm}>Password</Text>
//                         <TextInput style={coinDetailStyle.textInputPassword}
//                             autoCorrect={false}
//                             placeholder='Password'
//                             secureTextEntry={true}
//                             onChangeText={(text) => { this.setState({ password: text }) }}
//                         ></TextInput>
//                     </View>
//                     <View style={coinDetailStyle.containerInput}>
//                         <Text style={coinDetailStyle.textForm}>Name</Text>
//                         <TextInput style={coinDetailStyle.textInputPassword}
//                             autoCorrect={false}
//                             placeholder='Name'
//                             onChangeText={(text) => { this.setState({ name: text }) }}
//                         ></TextInput>
//                     </View>
//                     <View style={coinDetailStyle.containerInput}>
//                         <Text style={coinDetailStyle.textForm}>Last Name</Text>
//                         <TextInput style={coinDetailStyle.textInputPassword}
//                             autoCorrect={false}
//                             placeholder='Last Name'
//                             onChangeText={(text) => { this.setState({ lname: text }) }}
//                         ></TextInput>
//                     </View>
//                     <View style={coinDetailStyle.containerInput}>
//                         <Text style={coinDetailStyle.textForm}>License</Text>
//                         <TextInput style={coinDetailStyle.textInputPassword}
//                             autoCorrect={false}
//                             placeholder='License'
//                             onChangeText={(text) => { this.setState({ license: text }) }}
//                         ></TextInput>
//                     </View>
//                     <Button
//                         title="SAVE"
//                         onPress={() => {
//                             this.addAccount();
//                         }}
//                     />
//                 </View>

//             </View>

//         );
//     }
// }

// const coinDetailStyle = {
//     containerMain: {
//         flexDirection: 'column',
//         flex: 1,
//         marginTop: 100
//     },
//     containerInput: {
//         flexDirection: 'row',
//         marginBottom: 6,
//     },
//     textForm: {
//         fontSize: 18,
//         flex: 1,
//     },
//     textInputEmail: {
//         padding: 4,
//         fontSize: 18,
//         height: 30,
//         width: 100,
//         borderColor: '#DDDDDD',
//         borderWidth: 1,
//         flex: 2,
//     },
//     textInputPassword: {
//         padding: 4,
//         fontSize: 18,
//         height: 30,
//         width: 100,
//         borderColor: '#DDDDDD',
//         borderWidth: 1,
//         flex: 2
//     },
//     buttonLogin: {
//         marginTop: 20,
//         fontSize: 20,
//         alignContent: 'center',
//     }

// };

