
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  CheckBox,
  Image,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import {
  Form,
  Icon,
  Tab,
  Tabs,
  TabHeading,
  Header,
  Left,
  Right,
  Content,
  Container,
  Picker,
  Item,

} from 'native-base';

import { Router, Scene } from 'react-native-router-flux'
import styles from './styles';
import { Col, Row, Grid } from "react-native-easy-grid";
import { TextInput, Button, Searchbar } from 'react-native-paper';
import Tab1 from './Man/manStaff'
import Tab2 from './Man/manMenu'
import Tab3 from './Man/manPromotion'


export default class App extends Component {
  state = {
    value: 'first',
    type: undefined,
    type2: undefined,
    name: '',
    price: '',
    firstQuery: '',
    data: ['name', 'name']
  };
  static navigationOptions = {
    drawerIcon: ({ tintColor }) => (
      <Icon name="settings" style={{ fontSize: 24, color: tintColor }} />
    )
  }
  onValueChange2(value) {
    this.setState({
      type: value
    });
  }
  onValueChange(value) {
    this.setState({
      type2: value
    });
  }
  clear() {
    this.setState({
      type: undefined,
      type2: undefined,
      name: '',
      price: '',
    })
  }
  render() {
    const { firstQuery } = this.state;
    return (
      <View style={{ flex: 1 }}>

        <View style={{ justifyContent: 'flex-end' }}>
          <Header style={{ backgroundColor: '#29ab87' }}>
            <Left>
              <Icon name="ios-menu" onPress={() => this.props.navigation.openDrawer()} />
            </Left>
          </Header>
        </View>

        <Tabs tabContainerStyle={{ height: 60 }}>
          <Tab heading={
            <TabHeading>
              <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
              <Icon name="ios-people" style={{ fontSize: 30,textAlign: 'center' }} />
                <Text style={{ fontSize: 15, textAlign: 'center' }}>STAFF</Text>
              </View>
            </TabHeading>}>
            <Tab1

            />
          </Tab>

          <Tab heading={
            <TabHeading>
              <View style={{ flexDirection: 'column', }}>
              <Icon name="ios-add-circle" style={{ fontSize: 30 ,textAlign: 'center'}} />
                <Text style={{ fontSize: 15, textAlign: 'center' }}>MENU</Text>
              </View>
            </TabHeading>}>
            <Tab2

            />
          </Tab>

          <Tab heading={
            <TabHeading>
              <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
              <Icon name="ios-star" style={{ fontSize: 30, textAlign: 'center'}} />
                <Text style={{ fontSize: 15, textAlign: 'center' }}>PROMOTION</Text>
              </View>
            </TabHeading>}>
            <Tab3

            />
          </Tab>


        </Tabs>
      </View>
    );
  }
}


