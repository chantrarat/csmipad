// import React, { Component } from 'react';
// import {StyleSheet,Text,View,ListView} from 'react-native';
// import {createStore,combineReducers} from 'redux';
// import {Provider} from 'react-redux';
// import TestRedux from './testRedux';

// class App extends Component {


//   render(props) {
//     const reducerPerson = (state={firstname:'John',lastname:'Snow'},action)=>{
//       return state;
//     }

//     reducers = combineReducers({person:reducerPerson});
//     store = createStore(reducers);

//     return(
//       <Provider store={store}>
//         <View>
//           <TestRedux/>
//         </View>
//       </Provider>);
//   }
// }

// export default App;




import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import TestRedux from './testRedux';





export default class saleScreen extends Component {
    render(props) {

        const reducerPerson = (state = { firstname: 'John', lastname: 'Snow' }, action) => {
            return state;
        }
        
        reducers = combineReducers({ person: reducerPerson });
        store = createStore(reducers);
        return (
            <Provider store={store}>
                <View style={styles.container}>
                    <TestRedux />
                </View>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });