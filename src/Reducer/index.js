import { Item } from "native-base";




const initialState = {
    coffeeData: [],
    noncoffeeData: [],
    teaData: [],
    sodaData: [],
    cocoaData: [],
    milkData: [],
    bakeryData: [],
    orderList: [],
    bill: [],
    staffData:[]
}


const reducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case 'addItem': {
            let { orderList } = state
            return {
                ...state,
                orderList: [...orderList, payload]
            }
        }
        case 'clearItem': {
            let { orderList } = state
            orderList=[]
            return {
                ...state,
                orderList
            }
        }
        case 'addBill': {
            let { bill } = state
            console.log("bill",bill);
            
            return {
                ...state,
                bill: [...bill,payload]
                
            }
            
        }
        case 'updateList': {
            return {
                ...state,
                orderList: payload
            }
        }
        case 'setCoffeeData': {
            return {
                ...state,
                coffeeData: payload
            }
        }
        case 'setNonCoffeeData': {
            let {teaData,sodaData,cocoaData,milkData } = state
            console.log([...payload].filter(obj=>obj.type==="tea"));
            return {
                ...state,
                teaData: [...[...payload].filter(obj=>obj.type==="tea")],
                cocoaData: [...[...payload].filter(obj=>obj.type==="cocoa")],
                sodaData: [...[...payload].filter(obj=>obj.type==="soda")],
                milkData: [...[...payload].filter(obj=>obj.type==="milk")],
            }
        }
        case 'setBakeryData': {
            return {
                ...state,
                bakeryData: payload
            }
        }
    
        case 'getAll': {
            return {
                ...state
            }
        }
        default:
            return state
    }
}


export default reducer